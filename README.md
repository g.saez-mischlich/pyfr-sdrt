# PyFR-SDRT

PyFR-SDRT is a fork of the open-source PyFR solver developed by Gonzalo Sáez-Mischlich
which implements the SDRT method and FR-SDRT schemes for tensor-product and simplex
elements (excluding pyramids). See the article

Sáez-Mischlich, G., Sierra-Ausín, J., & Gressier, J. (2021).\
The Spectral Difference Raviart-Thomas method for two and three-dimensional elements and its connection with the Flux Reconstruction formulation.\
arXiv preprint arXiv:2105.08632.

for more information. This code is also capable of mixing FR and SDRT elements
within a same grid.

In order to use the SDRT method, one must add the following

```
[solver]
...
sd = True
```

to the input file. To use FR-SDRT one needs to use the following

```
[solver]
...
fr-sd = True
```

Both options are set to false by default.

This fork can also write VTU files during runtime using the following
(replacing `soln-plugin-writer` with `soln-plugin-writervtk`)

```
[soln-plugin-writervtk]
    dt-out = 0.01
    basedir = .
    basename = files-{t:.2f}
    post-action = echo "Wrote file {soln} at time {t} for mesh {mesh}."
    post-action-mode = blocking
    region = [(-5, -5, -5), (5, 5, 5)]
```

The license of this work remains consistent with that of the original PyFR solver

```
PyFR-SDRT is released under the New BSD License (see the LICENSE file for details).
Documentation is made available under a Creative Commons Attribution 4.0
license (see <http://creativecommons.org/licenses/by/4.0/>).
```

# PyFR

## Overview

PyFR is an open-source Python based framework for solving advection-diffusion
type problems on streaming architectures using the Flux Reconstruction
approach of Huynh. The framework is designed to solve a range of governing
systems on mixed unstructured grids containing various element types. It is
also designed to target a range of hardware platforms via use of an in-built
domain specific language derived from the Mako templating engine.

## Examples

Examples of using PyFR are available in the `examples` directory. Currently
available examples are:

- 2D Couette flow
- 2D Euler vortex
- 2D incompressible cylinder flow

## Contributing

To contribute to PyFR please follow the steps listed below:

1. Fork this repository to your GitHub account
2. Create a new branch in your forked repository
3. Make changes in your new branch
4. Submit your changes by creating a Pull Request to the `develop` branch of the original PyFR repository

Modifications to the `develop` branch are eventually merged into the master
branch for a new release.

## Help and Community

Information on the usage and stucture of PyFR can be found in the [user guide](http://pyfr.org/user_guide.php)
and [developer guide](http://pyfr.org/developer_guide.php).

For further help in developing and using PyFR, or to connect with the wider
community of users, please go to the [PyFR Discourse](https://pyfr.discourse.group). 

## Authors

See the AUTHORS file.

## License

PyFR is released under the New BSD License (see the LICENSE file for details).
Documentation is made available under a Creative Commons Attribution 4.0
license (see <http://creativecommons.org/licenses/by/4.0/>).
