# -*- coding: utf-8 -*-

import itertools as it
from math import exp
import re

import numpy as np

from pyfr.nputil import block_diag, clean
from pyfr.polys import get_polybasis
from pyfr.polys_rt import get_rt_polybasis
from pyfr.quadrules import get_quadrule
from pyfr.util import lazyprop


def _proj_pts(projector, pts):
    pts = np.atleast_2d(pts.T)
    return np.vstack(np.broadcast_arrays(*projector(*pts))).T


@clean
def _proj_l2(qrule, basis):
    return basis.vdm.T @ (qrule.wts*basis.ortho_basis_at(qrule.pts))


class BaseShape(object):
    name = None
    ndims = -1

    npts_coeffs = None
    npts_cdenom = None

    npts_for_face = {
        'line': lambda order: order + 1,
        'tri': lambda order: (order + 1)*(order + 2) // 2,
        'quad': lambda order: (order + 1)**2
    }

    def __init__(self, nspts, cfg):
        self.nspts = nspts
        self.cfg = cfg
        self.order = cfg.getint('solver', 'order')

        self.antialias = cfg.get('solver', 'anti-alias', 'none')
        self.antialias = {s.strip() for s in self.antialias.split(',')}
        self.antialias.discard('none')
        if self.antialias - {'flux', 'surf-flux'}:
            raise ValueError('Invalid anti-alias options')

        self.ubasis = get_polybasis(self.name, self.order + 1, self.upts)

        self.sd = cfg.getbool('solver', 'sd', False)
        if self.sd and self.antialias:
            raise RuntimeError('Antialias and sd options are not compatible')

        if self.name in ['tri', 'quad', 'hex', 'tet', 'pri']:
            self.fsdbasis = get_rt_polybasis(self.name, self.order + 1,
                                             self.fintpts_stack, self.fpts,
                                             self.norm_fintpts, self.norm_fpts)
        else:
            # Even if SD is demanded, this element is not compatible
            # Do not raise an error, just use FR with this element
            self.sd = False

        # If SD we can use a metric basis one order higher,
        # due to the fact that
        # the flux basis is one order higher
        # than the solution basis
        self.add_order_mbasis = 0
        if self.cfg.getbool('solver', 'sd'):
            print('Using SDRT solver')
            if self.name == 'hex' or self.name == 'quad':
                # TODO: only add it all elements use SD
                self.add_order_mbasis = 1

        self.mbasis_order = max(self.order + 1 + self.add_order_mbasis, 2)

        if nspts:
            self.nsptsord = nsptord = self.order_from_nspts(nspts)
            self.sbasis = get_polybasis(self.name, nsptord, self.spts)

            # Basis for free-stream metric
            # We need p-th order pseudo grid points, which includes
            # p-th order points on faces.
            # It guarantees th q-th order collocation projection on the face
            # on the both adjacent cells.
            # Ref. 1 JCP 281, 28-54, Sec 4.2
            # Ref. 2 JSC 26(3), 301-327, Definition 1
            self.mbasis = get_polybasis(self.name, max(self.order + 1, 2),
                                        self.mpts)

    @classmethod
    def nspts_from_order(cls, sptord):
        return int(np.polyval(cls.npts_coeffs, sptord)) // cls.npts_cdenom

    @classmethod
    def order_from_nspts(cls, nspts):
        # Obtain the coefficients for the poly: P(n) - nspts = 0
        coeffs = list(cls.npts_coeffs)
        coeffs[-1] -= cls.npts_cdenom*int(nspts)

        # Iterate
        for n in range(1, 15):
            if np.polyval(coeffs, n) == 0:
                return n
        else:
            raise ValueError('Invalid number of shape points')

    @clean
    def opmat(self, expr):
        expr = expr.lower().replace('*', '@')

        if not re.match(r'[m0-9\-+@() ]+$', expr):
            raise ValueError('Invalid operator matrix expression')

        mats = {m: getattr(self, m) for m in re.findall(r'm\d+', expr)}
        return eval(expr, {'__builtins__': None}, mats)

    @lazyprop
    def m0(self):
        return self.ubasis.nodal_basis_at(self.fpts)

    @lazyprop
    def m1(self):
        m = np.rollaxis(self.ubasis.jac_nodal_basis_at(self.upts), 2)
        return m.reshape(self.nupts, -1)

    @lazyprop
    def m2(self):
        m = self.norm_fpts[..., None]*self.m0[:, None, :]
        return m.reshape(self.nfpts, -1)

    @lazyprop
    def m3(self):
        m = self.gbasis_at(self.upts)

        if 'surf-flux' in self.antialias:
            fp = [_proj_l2(self._iqrules[kind], self.facebases[kind])
                  for kind, proj, norm in self.faces]

            m = m @ block_diag(fp)

        # This allows to transform FR-DG to FR-SDRT
        fr_sd = self.cfg.getbool('solver', 'fr-sd', False)
        if fr_sd:
            print('Using solverFR-SDRT')
            m = self.m14
        return m

    @lazyprop
    def m4(self):
        m = self.m1.reshape(self.nupts, -1, self.nupts).swapaxes(0, 1)
        return m.reshape(-1, self.nupts)

    @lazyprop
    def m6(self):
        m = self.norm_fpts.T[:, None, :]*self.m3
        return m.reshape(-1, self.nfpts)

    @lazyprop
    def m7(self):
        return self.ubasis.nodal_basis_at(self.qpts)

    @lazyprop
    def m8(self):
        return np.vstack([self.m0, self.m7])

    @lazyprop
    def m9(self):
        return _proj_l2(self._eqrule, self.ubasis)

    @property
    def m10(self):
        return block_diag([self.m9]*self.ndims)

    @lazyprop
    @clean
    def m11(self):
        ub = self.ubasis

        n = max(sum(dd) for dd in ub.degrees)
        ncut = self.cfg.getint('soln-filter', 'cutoff')
        order = self.cfg.getint('soln-filter', 'order')
        alpha = self.cfg.getfloat('soln-filter', 'alpha')

        A = np.ones(self.nupts)
        for i, d in enumerate(sum(dd) for dd in ub.degrees):
            if d >= ncut < n:
                A[i] = exp(-alpha*((d - ncut)/(n - ncut))**order)

        return np.linalg.solve(ub.vdm, A[:, None]*ub.vdm).T

    @lazyprop
    @clean
    def m12(self):
        # Interpolate from solution points to internal flux points
        m = self.ubasis.nodal_basis_at(self.fintpts)
        return m

    @lazyprop
    @clean
    def m13(self):
        # Contribution of internal flux points to the divergence
        # at solution points
        m = self.fsdbasis.div_basis_at(self.upts)
        return m[:, :self.ndims*self.nfintpts]

    @lazyprop
    @clean
    def m17(self):
        # Contribution from internal flux points to the divergence
        # at solution points
        # This differs from m13 because we suppose that there is a unique
        # component of the flux for each internal flux point

        # Remove columns with zeros in m13
        # Those are fluxes values which do not contribute to the divergence
        # This optimizes the performance with TensorProduct elements
        idxs = np.where(~self.m13.any(axis=0))[0]
        m = np.delete(self.m13, idxs, axis=1)
        return m

    @lazyprop
    @clean
    def m14(self):
        # Contribution of external flux points to the divergence
        # at solution points
        m = self.fsdbasis.div_basis_at(self.upts)
        return m[:, self.ndims*self.nfintpts:]

    @lazyprop
    @clean
    def m15(self):
        # m4
        # m = self.m1.reshape(self.nupts, -1, self.nupts).swapaxes(0, 1)
        # Gradient contribution from internal flux points
        # Reuse m13 matrix
        m = self.m13.reshape(self.nupts, -1, self.nfintpts).swapaxes(0, 1)
        return m.reshape(-1, self.nfintpts)

    @lazyprop
    @clean
    def m16(self):
        # m6
        # m = self.norm_fpts.T[:,None,:]*self.m4
        # Gradient contribution from external flux points
        m = self.norm_fpts.T[:, None, :]*self.m14
        return m.reshape(-1, self.nfpts)

    @lazyprop
    @clean
    def augmentation(self):
        idxs = [np.where((self.fintpts == f).all(axis=1))[0][0]
                for f in self.fintpts_stack]
        m = np.zeros((len(self.fintpts_stack), self.nfintpts))
        for j, idx in enumerate(idxs):
            m[j, idx] = 1
        return m

    @lazyprop
    def nfintpts(self):
        return len(self.fintpts)

    @lazyprop
    def nupts(self):
        n = self.order + 1
        return int(np.polyval(self.npts_coeffs, n)) // self.npts_cdenom

    @lazyprop
    def upts(self):
        rname = self.cfg.get(f'solver-elements-{self.name}', 'soln-pts')
        return get_quadrule(self.name, rname, self.nupts).pts

    def _get_qrule(self, eleint, kind, **kwargs):
        sect = f'solver-{eleint}-{kind}'

        if self.cfg.hasopt(sect, 'quad-pts'):
            kwargs['rule'] = self.cfg.get(sect, 'quad-pts')

        if self.cfg.hasopt(sect, 'quad-deg'):
            kwargs['qdeg'] = self.cfg.getint(sect, 'quad-deg')

        return get_quadrule(kind, **kwargs)

    @lazyprop
    def _eqrule(self):
        return self._get_qrule('elements', self.name)

    @lazyprop
    def _iqrules(self):
        return {kind: self._get_qrule('interfaces', kind, flags='s')
                for kind in {k for k, p, n in self.faces}}

    @property
    def qpts(self):
        return self._eqrule.pts

    @property
    def nqpts(self):
        return len(self.qpts)

    @lazyprop
    def fpts(self):
        ppts = []

        for kind, proj, norm in self.faces:
            # Obtain the flux points in reference space for the face type
            if 'surf-flux' in self.antialias:
                r = self._iqrules[kind]
            else:
                rule = self.cfg.get(f'solver-interfaces-{kind}', 'flux-pts')
                npts = self.npts_for_face[kind](self.order)

                r = get_quadrule(kind, rule, npts)

            # Project
            ppts.append(_proj_pts(proj, r.pts))

        return np.vstack(ppts)

    @lazyprop
    def fpts_wts(self):
        pwts = []

        for kind, proj, norm in self.faces:
            # Obtain the weights in reference space for the face type
            if 'surf-flux' in self.antialias:
                r = self._iqrules[kind]
            else:
                rule = self.cfg.get(f'solver-interfaces-{kind}', 'flux-pts')
                npts = self.npts_for_face[kind](self.order)

                r = get_quadrule(kind, rule, npts)

            pwts.append(r.wts)

        return np.hstack(pwts)

    @lazyprop
    def gbasis_coeffs(self):
        coeffs = []

        # Suitable quadrature rules for various face types
        qrule_map = {
            'line': ('gauss-legendre', self.order + 1),
            'quad': ('gauss-legendre', (self.order + 1)**2),
            'tri': ('williams-shunn', 36)
        }

        for kind, proj, norm in self.faces:
            # Obtain a quadrature rule for integrating on the reference face
            # and evaluate this rule at the nodal basis defined by the flux
            # points
            qr = get_quadrule(kind, *qrule_map[kind])
            L = self.facebases[kind].nodal_basis_at(qr.pts)

            # Do the quadrature
            M = self.ubasis.ortho_basis_at(_proj_pts(proj, qr.pts))
            S = np.einsum('i...,ik,ji->kj', qr.wts, L, M)

            coeffs.append(S)

        return np.vstack(coeffs)

    @clean
    def gbasis_at(self, pts):
        return (self.gbasis_coeffs @ self.ubasis.ortho_basis_at(pts)).T

    @property
    def facenorms(self):
        return [norm for kind, proj, norm in self.faces]

    @lazyprop
    def norm_fpts(self):
        fnorms = self.facenorms
        return np.vstack([[fn]*n for fn, n in zip(fnorms, self.nfacefpts)])

    @lazyprop
    def spts(self):
        return self.std_ele(self.nsptsord - 1)

    @lazyprop
    def linspts(self):
        return self.std_ele(1)

    @lazyprop
    def facebases(self):
        fb = {}

        for kind in {k for k, p, n in self.faces}:
            rule = self.cfg.get(f'solver-interfaces-{kind}', 'flux-pts')
            npts = self.npts_for_face[kind](self.order)

            pts = get_quadrule(kind, rule, npts).pts

            fb[kind] = get_polybasis(kind, self.order + 1, pts)

        return fb

    @lazyprop
    def facefpts(self):
        nf = np.cumsum([0] + self.nfacefpts)
        return [list(range(nf[i], nf[i + 1])) for i in range(len(nf) - 1)]

    @lazyprop
    def nfacefpts(self):
        if 'surf-flux' in self.antialias:
            def cnt(k): return len(self._iqrules[k].pts)
        else:
            def cnt(k): return self.npts_for_face[k](self.order)

        return [cnt(kind) for kind, proj, norm in self.faces]

    @property
    def nfpts(self):
        return sum(self.nfacefpts)

    @lazyprop
    def mpts(self):
        return self.std_ele(max(self.order, 1))

    @lazyprop
    def nmpts(self):
        return len(self.mpts)


class TensorProdShape(object):
    @classmethod
    def std_ele(cls, sptord):
        pts1d = np.linspace(-1, 1, sptord + 1)
        return [p[::-1] for p in it.product(pts1d, repeat=cls.ndims)]

    @lazyprop
    def fintpts(self):
        # Obtain internal flux points position for TensorProd
        # shapes

        kind = 'line'
        rule = self.cfg.get(f'solver-interfaces-{kind}', 'flux-pts')
        npts = self.npts_for_face[kind](self.order - 1)
        pts_fint = get_quadrule(kind, rule, npts).pts

        rule = self.cfg.get(f'solver-interfaces-{kind}', 'flux-pts')
        npts = self.npts_for_face[kind](self.order)
        pts_sol = get_quadrule(kind, rule, npts).pts
        pos = []
        for der in range(self.ndims):
            if self.ndims == 2:
                if der == 0:
                    pts = [[b, a] for a in pts_sol for b in pts_fint]
                else:
                    pts = [[a, b] for a in pts_sol for b in pts_fint]
            elif self.ndims == 3:
                if der == 0:
                    pts = [[c, a, b] for a in pts_sol for b in pts_sol
                           for c in pts_fint]
                elif der == 1:
                    pts = [[a, c, b] for a in pts_sol for b in pts_sol
                           for c in pts_fint]
                else:
                    pts = [[a, b, c] for a in pts_sol for b in pts_sol
                           for c in pts_fint]
            pos.append(pts)
        return np.vstack(pos)

    @lazyprop
    def fintpts_stack(self):
        # This supposes that the ndim flux at each external flux point
        # are ordered in row major, i.e.
        # [fint0_dim0, fint1_dim0, ..., fint0_dim1, fint1_dim1, ...]
        # This is a characteristic of PyFR
        return self.fintpts

    @lazyprop
    def norm_fintpts(self):
        # Return the dofs associated to each flux point
        # Each internal flux point has 1 associated dof
        # in TensorProd eles
        iden = np.eye(self.ndims)

        # Supposes that only one component of each
        # internal flux point contributes to the divergence
        # at solution points
        return np.repeat(iden, self.nfintpts // self.ndims, axis=0)


class QuadShape(TensorProdShape, BaseShape):
    name = 'quad'
    ndims = 2

    # nspts = n^2
    npts_coeffs = [1, 0, 0]
    npts_cdenom = 1

    # Faces: type, reference-to-face projection, normal
    faces = [
        ('line', lambda s: (s, -1), (0, -1)),
        ('line', lambda s: (1, s), (1, 0)),
        ('line', lambda s: (s, 1), (0, 1)),
        ('line', lambda s: (-1, s), (-1, 0)),
    ]

    # Jacobian expressions for a linear element
    jac_exprs = [
        ['((1 - x[1])*V[1][0] - (x[1] + 1)*V[2][0] +'
         ' (x[1] - 1)*V[0][0] + (x[1] + 1)*V[3][0])/4',
         '((1 - x[1])*V[1][1] - (x[1] + 1)*V[2][1] +'
         ' (x[1] - 1)*V[0][1] + (x[1] + 1)*V[3][1])/4'],
        ['((1 - x[0])*V[2][0] - (x[0] + 1)*V[1][0] +'
         ' (x[0] - 1)*V[0][0] + (x[0] + 1)*V[3][0])/4',
         '((1 - x[0])*V[2][1] - (x[0] + 1)*V[1][1] +'
         ' (x[0] - 1)*V[0][1] + (x[0] + 1)*V[3][1])/4']
    ]


class HexShape(TensorProdShape, BaseShape):
    name = 'hex'
    ndims = 3

    # nspts = n^3
    npts_coeffs = [1, 0, 0, 0]
    npts_cdenom = 1

    # Faces: type, reference-to-face projection, normal
    faces = [
        ('quad', lambda s, t: (s, t, -1), (0, 0, -1)),
        ('quad', lambda s, t: (s, -1, t), (0, -1, 0)),
        ('quad', lambda s, t: (1, s, t), (1, 0, 0)),
        ('quad', lambda s, t: (s, 1, t), (0, 1, 0)),
        ('quad', lambda s, t: (-1, s, t), (-1, 0, 0)),
        ('quad', lambda s, t: (s, t, 1), (0, 0, 1)),
    ]

    # Jacobian expressions for a linear element
    jac_exprs = [
        [f'((-x[1]*x[2] + x[1] + x[2] - 1)*V[0][{i}] +'
         f' ( x[1]*x[2] - x[1] - x[2] + 1)*V[1][{i}] +'
         f' ( x[1]*x[2] - x[1] + x[2] - 1)*V[2][{i}] +'
         f' (-x[1]*x[2] + x[1] - x[2] + 1)*V[3][{i}] +'
         f' ( x[1]*x[2] + x[1] - x[2] - 1)*V[4][{i}] +'
         f' (-x[1]*x[2] - x[1] + x[2] + 1)*V[5][{i}] +'
         f' (-x[1]*x[2] - x[1] - x[2] - 1)*V[6][{i}] +'
         f' ( x[1]*x[2] + x[1] + x[2] + 1)*V[7][{i}])/8'
         for i in range(3)],
        [f'((-x[0]*x[2] + x[0] + x[2] - 1)*V[0][{i}] +'
         f' ( x[0]*x[2] - x[0] + x[2] - 1)*V[1][{i}] +'
         f' ( x[0]*x[2] - x[0] - x[2] + 1)*V[2][{i}] +'
         f' (-x[0]*x[2] + x[0] - x[2] + 1)*V[3][{i}] +'
         f' ( x[0]*x[2] + x[0] - x[2] - 1)*V[4][{i}] +'
         f' (-x[0]*x[2] - x[0] - x[2] - 1)*V[5][{i}] +'
         f' (-x[0]*x[2] - x[0] + x[2] + 1)*V[6][{i}] +'
         f' ( x[0]*x[2] + x[0] + x[2] + 1)*V[7][{i}])/8'
         for i in range(3)],
        [f'((-x[0]*x[1] + x[0] + x[1] - 1)*V[0][{i}] +'
         f' ( x[0]*x[1] - x[0] + x[1] - 1)*V[1][{i}] +'
         f' ( x[0]*x[1] + x[0] - x[1] - 1)*V[2][{i}] +'
         f' (-x[0]*x[1] - x[0] - x[1] - 1)*V[3][{i}] +'
         f' ( x[0]*x[1] - x[0] - x[1] + 1)*V[4][{i}] +'
         f' (-x[0]*x[1] + x[0] - x[1] + 1)*V[5][{i}] +'
         f' (-x[0]*x[1] - x[0] + x[1] + 1)*V[6][{i}] +'
         f' ( x[0]*x[1] + x[0] + x[1] + 1)*V[7][{i}])/8'
         for i in range(3)]
    ]


class TriShape(BaseShape):
    name = 'tri'
    ndims = 2

    # nspts = n*(n + 1)/2
    npts_coeffs = [1, 1, 0]
    npts_cdenom = 2

    # Faces: type, reference-to-face projection, normal
    faces = [
        ('line', lambda s: (s, -1), (0, -1)),
        ('line', lambda s: (-s, s), (1, 1)),
        ('line', lambda s: (-1, s), (-1, 0)),
    ]

    # Jacobian expressions for a linear element
    jac_exprs = [
        [f'(V[{i + 1}][{j}] - V[0][{j}])/2' for j in range(2)]
        for i in range(2)
    ]

    @classmethod
    def std_ele(cls, sptord):
        pts1d = np.linspace(-1, 1, sptord + 1)

        return [(p, q)
                for i, q in enumerate(pts1d)
                for p in pts1d[:(sptord + 1 - i)]]

    @lazyprop
    def fintpts(self):
        if self.order > 6:
            raise RuntimeError(f'Solution order {self.order} '
                               f'not compatible with SDM and {self.name}')
        npts_order = {1: 1, 2: 3, 3: 6, 4: 10, 5: 15, 6: 21}
        r = get_quadrule('tri', 'williams-shunn', npts=npts_order[self.order])

        return np.array(r.pts)

    @lazyprop
    def fintpts_stack(self):
        # This supposes that the ndim flux at each external flux point
        # are ordered in row major, i.e.
        # [fint0_dim0, fint1_dim0, ..., fint0_dim1, fint1_dim1, ...]
        # This is a characteristic of PyFR
        return np.vstack([self.fintpts]*self.ndims)

    @lazyprop
    def norm_fintpts(self):
        # Return the dofs associated to each flux point
        # Each internal flux point has self.ndims associated dofs
        # For example in 3D
        # (1, 0, 0) ; (0, 1, 0) and (0, 0, 1)
        iden = np.eye(self.ndims)

        # Supposes that the ndim flux at each external flux point
        # are ordered in row major, i.e.
        # [fint0_dim0, fint1_dim0, ..., fint0_dim1, fint1_dim1, ...]
        # This is a characteristic of PyFR
        return np.repeat(iden, self.nfintpts, axis=0)


class TetShape(TriShape, BaseShape):
    name = 'tet'
    ndims = 3

    # nspts = n*(n + 1)*(n + 2)/6
    npts_coeffs = [1, 3, 2, 0]
    npts_cdenom = 6

    # Faces: type, reference-to-face projection, normal
    faces = [
        ('tri', lambda s, t: (s, t, -1), (0, 0, -1)),
        ('tri', lambda s, t: (s, -1, t), (0, -1, 0)),
        ('tri', lambda s, t: (-1, t, s), (-1, 0, 0)),
        ('tri', lambda s, t: (s, t, -s - t - 1), (1, 1, 1)),
    ]

    # Jacobian expressions for a linear element
    jac_exprs = [
        [f'(V[{i + 1}][{j}] - V[0][{j}])/2' for j in range(3)]
        for i in range(3)
    ]

    @classmethod
    def std_ele(cls, sptord):
        pts1d = np.linspace(-1, 1, sptord + 1)

        return [(p, q, r)
                for i, r in enumerate(pts1d)
                for j, q in enumerate(pts1d[:(sptord + 1 - i)])
                for p in pts1d[:(sptord + 1 - i - j)]]

    @lazyprop
    def fintpts(self):
        if self.order > 6:
            raise RuntimeError(f'Solution order {self.order} '
                               f'not compatible with SDM and {self.name}')
        npts_order = {1: 1, 2: 4, 3: 10, 4: 20, 5: 35, 6: 56}
        r = get_quadrule('tet', 'shunn-ham', npts=npts_order[self.order])

        return np.array(r.pts)


class PriShape(BaseShape):
    name = 'pri'
    ndims = 3

    # nspts = n^2*(n + 1)/2
    npts_coeffs = [1, 1, 0, 0]
    npts_cdenom = 2

    # Faces: type, reference-to-face projection, normal
    faces = [
        ('tri', lambda s, t: (s, t, -1), (0, 0, -1)),
        ('tri', lambda s, t: (s, t, 1), (0, 0, 1)),
        ('quad', lambda s, t: (s, -1, t), (0, -1, 0)),
        ('quad', lambda s, t: (-s, s, t), (1, 1, 0)),
        ('quad', lambda s, t: (-1, s, t), (-1, 0, 0)),
    ]

    # Jacobian expressions for a linear element
    _jac_exprs_xy = [
        [f'((x[2] - 1)*V[0][{j}] + (1 - x[2])*V[{i + 1}][{j}] -'
         f' (x[2] + 1)*V[3][{j}] + (x[2] + 1)*V[{i + 4}][{j}])/4'
         for j in range(3)]
        for i in range(2)
    ]
    _jac_exprs_z = [[f'((x[0] + 1)*V[4][{j}] + (x[0] + x[1])*V[0][{j}] -'
                     f' (x[0] + 1)*V[1][{j}] - (x[0] + x[1])*V[3][{j}] -'
                     f' (x[1] + 1)*V[2][{j}] + (x[1] +    1)*V[5][{j}])/4'
                     for j in range(3)]]
    jac_exprs = _jac_exprs_xy + _jac_exprs_z

    @classmethod
    def std_ele(cls, sptord):
        pts1d = np.linspace(-1, 1, sptord + 1)

        return [(p, q, r)
                for r in pts1d
                for i, q in enumerate(pts1d)
                for p in pts1d[:(sptord + 1 - i)]]

    @lazyprop
    def fintpts(self):
        if self.order > 6:
            raise RuntimeError(f'Solution order {self.order} '
                               f'not compatible with SDM and {self.name}')

        # Build the flux points which are not related to the upper or lower
        # tri polygon base
        # These ones are similar to those of tris in 2D SDRT
        # but extruded in the z direction to coincide
        # with the Gauss-Legendre quadrature points of quad faces
        rname = self.cfg.get(f'solver-interfaces-tri', 'flux-pts')
        n = self.order
        n = np.polyval(TriShape.npts_coeffs, n) // self.npts_cdenom
        r = get_quadrule('tri', rname, npts=n)
        fintpts_notris = r.pts

        # Get the Gauss-Legendre quadratures
        rule = self.cfg.get(f'solver-interfaces-quad', 'flux-pts')
        npts = self.npts_for_face['quad'](self.order)
        r = get_quadrule('quad', rule, npts)

        # Get the z extrusion points
        pts_quad_order = np.unique(r.pts)

        # Add z extrusion of the the points
        fintpts_notris = [list(a) + [b] for a in fintpts_notris
                          for b in pts_quad_order]

        # Now build the replications of the flux points
        # of the tris located at the prism geometrical basis

        # Need to extrude this poins, similarly as it is done with
        # hexa and quad in the pure SDM
        npts = self.npts_for_face['quad'](self.order - 1)
        r = get_quadrule('quad', rule, npts)
        pts_quad_orderm1 = np.unique(r.pts)

        # Find the tri flux/quadrature points associated
        # to the tri faces of the prism
        n = self.order + 1
        n = np.polyval(TriShape.npts_coeffs, n) // self.npts_cdenom
        r = get_quadrule('tri', rname, npts=n)
        fintpts_tris = r.pts

        # Extrude in the z direction
        fintpts_tris = [list(a) + [b] for a in fintpts_tris
                        for b in pts_quad_orderm1]

        # Store some info
        self.nfintpts_notris = len(fintpts_notris)
        self.nfintpts_tris = len(fintpts_tris)
        self.fintpts_notris = fintpts_notris
        self.fintpts_tris = fintpts_tris

        # First we have the internal flux points that do not
        # replicate the tri polygonal base (those may only have
        # [1, 0, 0] and [0, 1, 0] as dofs)
        # And then we add the dofs duplicating the tri polygonal
        # base. Those have [0, 0, 1] as dof.
        fintpts = np.vstack([fintpts_notris, fintpts_tris])
        return np.array(fintpts)

    @lazyprop
    def fintpts_stack(self):
        # This supposes that the ndim flux at each external flux point
        # are ordered in row major, i.e.
        # [fint0_dim0, fint1_dim0, ..., fint0_dim1, fint1_dim1, ...]
        # This is a characteristic of PyFR

        # Compute fintpts
        self.fintpts

        # First stack the notris
        # and the stack the tris
        # 2* because each notri flux points has 2 degrees of freedom
        # 1* because each tri flux point has only 1 degree of freedom
        n = 2*self.nfintpts_notris + 1*self.nfintpts_tris
        pts = np.zeros((n, 3))

        # Fill in the flux points (duplicating the ones that use 2
        # degrees of freedom)
        pts[:2*self.nfintpts_notris, :] = np.vstack([self.fintpts_notris]*2)

        # Add those with only 1 degree of freedom
        pts[2*self.nfintpts_notris:, :] = self.fintpts_tris

        return pts

    @lazyprop
    def norm_fintpts(self):
        # Return the dofs associated to each flux point
        # Each internal flux point has self.ndims associated dofs
        # For example in 3D
        # (1, 0, 0) ; (0, 1, 0) and (0, 0, 1)

        # First stack the notris
        # and the stack the tris
        n = 2*self.nfintpts_notris + 1*self.nfintpts_tris
        dofs = np.zeros((n, 3))

        # notris have [1, 0, 0] and [0, 0, 1] as dofs
        dofs[:self.nfintpts_notris, :] = [1, 0, 0]
        dofs[self.nfintpts_notris:2*self.nfintpts_notris, :] = [0, 1, 0]

        # tris have [0, 0, 1] as dofs
        dofs[2*self.nfintpts_notris:, :] = [0, 0, 1]

        # Supposes that the ndim flux at each external flux point
        # are ordered in row major, i.e.
        # [fint0_dim0, fint1_dim0, ..., fint0_dim1, fint1_dim1, ...]
        # This is a characteristic of PyFR
        return dofs


class PyrShape(BaseShape):
    name = 'pyr'
    ndims = 3

    # nspts = n*(n + 1)*(2*n + 1)/6
    npts_coeffs = [2, 3, 1, 0]
    npts_cdenom = 6

    # Faces: type, reference-to-face projection, normal
    faces = [
        ('quad', lambda s, t: (s, t, -1), (0, 0, -1)),
        ('tri', lambda s, t: (s + (t + 1)/2, (t - 1)/2, t), (0, -1, 0.5)),
        ('tri', lambda s, t: ((1 - t)/2, -s - (t + 1)/2, t), (1, 0, 0.5)),
        ('tri', lambda s, t: (-s - (t + 1)/2, (1 - t)/2, t), (0, 1, 0.5)),
        ('tri', lambda s, t: ((t - 1)/2, s + (t + 1)/2, t), (-1, 0, 0.5)),
    ]

    # Jacobian expressions for a linear element
    jac_exprs = [
        ['((1 - x[1])*V[1][0] + (-x[1] - 1)*V[2][0] +'
         ' (x[1] - 1)*V[0][0] + (x[1] + 1)*V[3][0])/4',
         '((1 - x[1])*V[1][1] + (-x[1] - 1)*V[2][1] +'
         '(x[1] - 1)*V[0][1] + (x[1] + 1)*V[3][1])/4',
         '((1 - x[1])*V[1][2] + (-x[1] - 1)*V[2][2] +'
         ' (x[1] - 1)*V[0][2] + (x[1] + 1)*V[3][2])/4'],
        ['((1 - x[0])*V[2][0] + (-x[0] - 1)*V[1][0] +'
         ' (x[0] - 1)*V[0][0] + (x[0] + 1)*V[3][0])/4',
         '((1 - x[0])*V[2][1] + (-x[0] - 1)*V[1][1] +'
         ' (x[0] - 1)*V[0][1] + (x[0] + 1)*V[3][1])/4',
         '((1 - x[0])*V[2][2] + (-x[0] - 1)*V[1][2] +'
         '(x[0] - 1)*V[0][2] + (x[0] + 1)*V[3][2])/4'],
        ['(-V[0][0] - V[1][0] - V[2][0] - V[3][0] + 4*V[4][0])/8',
         '(-V[0][1] - V[1][1] - V[2][1] - V[3][1] + 4*V[4][1])/8',
         '(-V[0][2] - V[1][2] - V[2][2] - V[3][2] + 4*V[4][2])/8']
    ]

    @classmethod
    def std_ele(cls, sptord):
        npts1d = 2*sptord + 1
        pts1d = np.linspace(-1, 1, npts1d)

        return [(p, q, r)
                for i, r in enumerate(pts1d[::2])
                for q in pts1d[i:npts1d - i:2]
                for p in pts1d[i:npts1d - i:2]]
