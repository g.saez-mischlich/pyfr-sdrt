# -*- coding: utf-8 -*-

from collections import defaultdict
import itertools as it
import re

import numpy as np

from pyfr.inifile import Inifile
from pyfr.shapes import BaseShape
from pyfr.util import proxylist, subclasses


class BaseSystem(object):
    elementscls = None
    intinterscls = None
    mpiinterscls = None
    bbcinterscls = None

    # Number of queues to allocate
    _nqueues = None

    # Nonce sequence
    _nonce_seq = it.count()

    def __init__(self, backend, rallocs, mesh, initsoln, nregs, cfg):
        self.backend = backend
        self.mesh = mesh
        self.cfg = cfg

        # Obtain a nonce to uniquely identify this system
        nonce = str(next(self._nonce_seq))

        # Load the elements
        eles, elemap = self._load_eles(rallocs, mesh, initsoln, nregs, nonce)
        backend.commit()

        # Retain the element map; this may be deleted by clients
        self.ele_map = elemap

        # Get the banks, types, num DOFs and shapes of the elements
        self.ele_banks = list(eles.scal_upts_inb)
        self.ele_types = list(elemap)
        self.ele_ndofs = [e.neles*e.nupts*e.nvars for e in eles]
        self.ele_shapes = [(e.nupts, e.nvars, e.neles) for e in eles]

        # Get all the solution point locations for the elements
        self.ele_ploc_upts = [e.ploc_at_np('upts') for e in eles]

        # I/O banks for the elements
        self.eles_scal_upts_inb = eles.scal_upts_inb
        self.eles_scal_upts_outb = eles.scal_upts_outb
        self.eles_vect_upts = eles._vect_upts

        # Save the number of dimensions and field variables
        self.ndims = eles[0].ndims
        self.nvars = eles[0].nvars

        # Load the interfaces
        int_inters = self._load_int_inters(rallocs, mesh, elemap)
        mpi_inters = self._load_mpi_inters(rallocs, mesh, elemap)
        bc_inters = self._load_bc_inters(rallocs, mesh, elemap)
        backend.commit()

        # Prepare the queues and kernels
        self._gen_queues()
        self._gen_kernels(eles, int_inters, mpi_inters, bc_inters)
        backend.commit()

        # Save the BC interfaces, but delete the memory-intensive elemap
        self._bc_inters = bc_inters
        del bc_inters.elemap

    def _load_eles(self, rallocs, mesh, initsoln, nregs, nonce):
        # TODO: just_leaf should be True, but this means that
        # TetShape can not inherit from TriShape
        basismap = {b.name: b for b in subclasses(BaseShape, just_leaf=False)}

        # Look for and load each element type from the mesh
        elemap = {}
        for f in mesh:
            if (m := re.match(f'spt_(.+?)_p{rallocs.prank}$', f)):
                # Element type
                t = m.group(1)

                elemap[t] = self.elementscls(basismap[t], mesh[f], self.cfg)

        # Construct a proxylist to simplify collective operations
        eles = proxylist(elemap.values())

        # Set the initial conditions
        if initsoln:
            # Load the config and stats files from the solution
            solncfg = Inifile(initsoln['config'])
            solnsts = Inifile(initsoln['stats'])

            # Get the names of the conserved variables (fields)
            solnfields = solnsts.get('data', 'fields', '')
            currfields = ','.join(eles[0].convarmap[eles[0].ndims])

            # Ensure they match up
            if solnfields and solnfields != currfields:
                raise RuntimeError('Invalid solution for system')

            # Process the solution
            for etype, ele in elemap.items():
                soln = initsoln[f'soln_{etype}_p{rallocs.prank}']
                ele.set_ics_from_soln(soln, solncfg)
        else:
            eles.set_ics_from_cfg()

        # Allocate these elements on the backend
        for etype, ele in elemap.items():
            k = f'spt_{etype}_p{rallocs.prank}'

            try:
                curved = ~mesh[k, 'linear']
                linoff = np.max(*np.nonzero(curved), initial=-1) + 1
            except KeyError:
                linoff = ele.neles

            ele.set_backend(self.backend, nregs, nonce, linoff)

        return eles, elemap

    def _load_int_inters(self, rallocs, mesh, elemap):
        key = f'con_p{rallocs.prank}'

        lhs, rhs = mesh[key].astype('U4,i4,i1,i2').tolist()
        int_inters = self.intinterscls(self.backend, lhs, rhs, elemap,
                                       self.cfg)

        # Although we only have a single internal interfaces instance
        # we wrap it in a proxylist for consistency
        return proxylist([int_inters])

    def _load_mpi_inters(self, rallocs, mesh, elemap):
        lhsprank = rallocs.prank

        mpi_inters = proxylist([])
        for rhsprank in rallocs.prankconn[lhsprank]:
            rhsmrank = rallocs.pmrankmap[rhsprank]
            interarr = mesh[f'con_p{lhsprank}p{rhsprank}']
            interarr = interarr.astype('U4,i4,i1,i2').tolist()

            mpiiface = self.mpiinterscls(self.backend, interarr, rhsmrank,
                                         rallocs, elemap, self.cfg)
            mpi_inters.append(mpiiface)

        return mpi_inters

    def _load_bc_inters(self, rallocs, mesh, elemap):
        bccls = self.bbcinterscls
        bcmap = {b.type: b for b in subclasses(bccls, just_leaf=True)}

        bc_inters = proxylist([])
        for f in mesh:
            if (m := re.match(f'bcon_(.+?)_p{rallocs.prank}$', f)):
                # Get the region name
                rgn = m.group(1)

                # Determine the config file section
                cfgsect = f'soln-bcs-{rgn}'

                # Get the interface
                interarr = mesh[f].astype('U4,i4,i1,i2').tolist()

                # Instantiate
                bcclass = bcmap[self.cfg.get(cfgsect, 'type')]
                bciface = bcclass(self.backend, interarr, elemap, cfgsect,
                                  self.cfg)
                bc_inters.append(bciface)

        return bc_inters

    def _gen_queues(self):
        self._queues = [self.backend.queue() for i in range(self._nqueues)]

    def _gen_kernels(self, eles, iint, mpiint, bcint):
        self._kernels = kernels = defaultdict(list)

        provnames = ['eles', 'iint', 'mpiint', 'bcint']
        provobjs = [eles, iint, mpiint, bcint]

        for pn, pobj in zip(provnames, provobjs):
            for kn, kgetter in it.chain(*pobj.kernels.items()):
                if not kn.startswith('_'):
                    kernels[pn, kn].append(kgetter())

    def rhs(self, t, uinbank, foutbank):
        pass

    def compute_grads(self, t, uinbank):
        raise NotImplementedError(f'Solver "{self.name}" does not compute '
                                  'corrected gradients of the solution')

    def filt(self, uinoutbank):
        self.eles_scal_upts_inb.active = uinoutbank

        self._queues[0].enqueue_and_run(self._kernels['eles', 'filter_soln'])

    def ele_scal_upts(self, idx):
        return [eb[idx].get() for eb in self.ele_banks]

    def compute_jacobian(self, t, uinbank, foutbank):
        # Compute the jacobian of a given solver
        # WARNING: only works in serial

        basismap = {b.name: b for b in subclasses(BaseShape, just_leaf=False)}
        # Look for and load each element type from the mesh
        elemap = {}
        for f in self.mesh:
            m = re.match(f'spt_(.+?)_p0$', f)
            if m:
                # Element type
                t = m.group(1)

                elemap[t] = self.elementscls(basismap[t], self.mesh[f], self.cfg)

        # Construct a proxylist to simplify collective operations
        eles = proxylist(elemap.values())

        ele_types = elemap.keys()

        neles = {t: -1 for t in ele_types}
        eles_nupts_type = {t: -1 for t in ele_types}
        total_neles = sum([ele.neles for ele in eles])
        total_nupts = sum([ele.neles*ele.nupts for ele in eles])
        eles_nupts = []
        upts_localidx = np.zeros(total_nupts)
        idxb_types = {t: -1 for t in ele_types}
        idx_insert = 0
        for i, el in enumerate(eles):
            idx = 0
            ele_type = el.basis.name
            idxb_types[ele_type] = i
            neles[ele_type] = el.neles
            eles_nupts_type[ele_type] = el.nupts
            for j in range(el.neles):
                eles_nupts.append(el.nupts)
                for k in range(el.nupts):
                    upts_localidx[idx_insert] = idx
                    idx += 1
                    idx_insert += 1

        # Compute number of elements per element type
        for l, r in zip(*self.mesh['con_p0'].tolist()):
            leletype, leleidx, lfidx = l[:-1]
            if leleidx > neles[leletype.decode()]:
                neles[leletype] = leleidx

        # Store direct face neighbors
        neighbors = {t: [[] for x in range(neles[t])] for t in ele_types}
        for l, r in zip(*self.mesh['con_p0'].tolist()):
            leletype, leleidx, lfidx = l[:-1]
            reletype, releidx, rfidx = r[:-1]

            neighbors[leletype.decode()][leleidx].append((reletype, releidx))
            neighbors[reletype.decode()][releidx].append((leletype, leleidx))

        # Append the root cell to its neighbors array
        # For each type of element
        for el in eles:
            # For each element in element type
            for ele in range(el.neles):
                # Append this cell to its own neighbors array
                key = (el.basis.name.encode(), ele)
                neighbors[el.basis.name][ele].append(key)

        # Remove repeated entries
        # For each type of element
        for el in eles:
            # For each element in element type
            for ele in range(el.neles):
                arr = list(set(neighbors[el.basis.name][ele]))
                neighbors[el.basis.name][ele] = arr

        ldg = self.cfg.get('solver-interfaces', 'ldg-beta', '')
        if ldg != '':
            # In this case the stencil is not compact
            # even with \beta = +-0.5
            # Add neighbors of neighbors
            print('Extending stencils due to viscous Riemann solver')

            # Copy the previous neighbors
            # to avoid overwriting
            import copy
            neighbors_cpy = copy.deepcopy(neighbors)

            # For each element type
            for el in eles:
                # For each element in element type
                for ele in range(el.neles):
                    # Add neighbors of neighbors
                    for neigh in neighbors_cpy[el.basis.name][ele]:
                        neigh_type = neigh[0].decode()
                        neigh_idx = neigh[1]
                        add = neighbors_cpy[neigh_type][neigh_idx]
                        neighbors[el.basis.name][ele].extend(add)

        # Remove duplicated entries
        # For each type of element
        for el in eles:
            # For each element in element type
            for ele in range(el.neles):
                arr = list(set(neighbors[el.basis.name][ele]))
                neighbors[el.basis.name][ele] = arr

        # Jacobian matrix
        jac = np.zeros((total_nupts, total_nupts), dtype=np.float64)

        # Perturbation
        eps = 1.0

        # Activate in and out solution banks
        self.eles_scal_upts_inb.active = uinbank
        self.eles_scal_upts_outb.active = foutbank

        upts_out = np.zeros((total_nupts, eles[0].ndims))
        # Compute RHS Jacobian

        max_res = -1.0

        # If performing a diffusion/dispersion analysis
        # only the rows of the RHS Jacobian associated
        # to the root pattern cell need to be computed
        # This root pattern cell is located at
        # the center of the domain
        only_root_pattern = True
        if only_root_pattern:
            # Only compatible with meshes made up of a unique
            # type of elements
            if len(eles) > 1:
                raise ValueError('only_root_pattern activated but more than '
                                 'one type of elements found')

            # Compute the center of the domain
            points = self.ele_ploc_upts[0]
            center_domain = points.sum(axis=0).sum(axis=1)
            center_domain /= (points.shape[0]*points.shape[2])
            print(f'{center_domain=}')

        # Index of the solution point
        idx = 0
        # For each type of element
        for j, el in enumerate(eles):
            # For each element in element type
            for ele in range(el.neles):
                print(f'{el.basis.name} {ele=} of {el.neles}')
                print(f'{len(neighbors[el.basis.name][ele])=}')
                if only_root_pattern:
                    # If this element is not the root cell
                    # there is no need to compute the rows of the
                    # Jacobian associated to this element
                    h = 1.0
                    min_x, max_x = center_domain[0] - 0.5*h, center_domain[0] + 0.5*h
                    min_y, max_y = center_domain[1] - 0.5*h, center_domain[1] + 0.5*h
                    points = self.ele_ploc_upts[j][:, :, ele]

                    bound_x = np.logical_and(points[:, 0] > min_x, points[:, 0] < max_x)
                    bound_y = np.logical_and(points[:, 1] > min_y, points[:, 1] < max_y)
                    if eles[0].ndims == 2:
                        bb_filter = np.logical_and(bound_x, bound_y)
                    else:
                        min_z, max_z = center_domain[2] - 0.5*h, center_domain[2] + 0.5*h
                        bound_z = np.logical_and(points[:, 2] > min_z, points[:, 2] < max_z)
                        bb_filter = np.logical_and(np.logical_and(bound_x, bound_y), bound_z)
                    idxs = np.where(bb_filter == True)[0]
                    if len(idxs) == 0:
                        # This element is not the root cell
                        # Annotate the upts and increment the idx
                        # variable
                        for upt in range(el.nupts):
                            upts_out[idx] = self.ele_ploc_upts[j][upt, :, ele]
                            # Next solution point to be analyzed
                            idx += 1
                        # Go to the next element
                        continue
                # For each solution point of this cell
                for upt in range(el.nupts):
                    # For each neighbor of this cell
                    for neigh in neighbors[el.basis.name][ele]:
                        # Neighbor element type
                        neigh_type = neigh[0].decode()

                        # Neighbor number of solution points
                        nupts_neigh = elemap[neigh_type].nupts

                        # Neighbor index (relative to its own type)
                        # i.e., each element type starts at 0
                        neigh_index = neigh[1]

                        # Start idx of the solution points of the neighbor
                        # cell in the Jacobian matrix
                        idx_upts_neigh = 0
                        for k, key in enumerate(ele_types):
                            if key == neigh_type:
                                break
                            idx_upts_neigh += eles[k].neles*eles[k].nupts
                        idx_upts_neigh += neigh_index*nupts_neigh

                        # For each solution point of the neighbor
                        for upt2 in range(nupts_neigh):
                            # Zeroth all the RHS and banks utilized
                            # Not needed for all banks but well,
                            # we ensure it at least...
                            for i, key in enumerate(ele_types):
                                arr = self.eles_scal_upts_inb[i][uinbank]
                                shape = arr.ioshape
                                z = np.zeros(shape)

                                self.eles_scal_upts_inb[i][uinbank]._set(z)
                                self.eles_scal_upts_outb[i][foutbank]._set(z)

                            # Bank of the cell being analyzed
                            idxb = idxb_types[el.basis.name]

                            # Bank of the neighbor
                            idxb_neigh = idxb_types[neigh_type]

                            # Modify the in bank of the neighbor solution point
                            # Add eps to its value
                            arr = self.eles_scal_upts_inb[idxb_neigh][uinbank]
                            shape = arr.ioshape
                            a = np.zeros(shape)
                            a[upt2, 0, neigh_index] = eps
                            arr._set(a)

                            # Compute RHS
                            self.rhs(t, uinbank, foutbank)

                            # Compute Jacobian
                            # INFO: supposing full periodic cases, the initial
                            # RHS can be taken as zero
                            rhsb = self.eles_scal_upts_outb[idxb][foutbank]
                            der = rhsb._get()[upt, 0, ele]/eps

                            # Update Jacobian Matrix
                            jac[idx, idx_upts_neigh + upt2] = der

                    upts_out[idx] = self.ele_ploc_upts[j][upt, :, ele]

                    # Ensure that the rows of the Jacobian are appropriate
                    tol = 1e-10
                    res = np.max(np.sum(jac[idx, :]))
                    if abs(res) > tol:
                        raise RuntimeError(f'Error jac {res}')
                    if res > max_res:
                        max_res = res

                    # Next solution point to be analyzed
                    idx += 1
        print(f'{max_res=}')
        np.save('jac.npy', jac)
        np.savetxt('upts.csv', upts_out, delimiter=",")
        exit()
