# -*- coding: utf-8 -*-

import math
import re

import numpy as np

from pyfr.nputil import npeval, fuzzysort
from pyfr.util import lazyprop, memoize


class BaseElements(object):
    privarmap = None
    convarmap = None

    def __init__(self, basiscls, eles, cfg):
        self._be = None

        self.eles = eles
        self.cfg = cfg

        self.nspts = nspts = eles.shape[0]
        self.neles = neles = eles.shape[1]
        self.ndims = ndims = eles.shape[2]

        # Kernels we provide
        self.kernels = {}

        # Check the dimensionality of the problem
        if ndims != basiscls.ndims or ndims not in self.privarmap:
            raise ValueError('Invalid element matrix dimensions')

        # Determine the number of dynamical variables
        self.nvars = len(self.privarmap[ndims])

        # Instantiate the basis class
        self.basis = basis = basiscls(nspts, cfg)

        # See what kind of projection the basis is using
        self.antialias = basis.antialias

        # If we need quadrature points or not
        haveqpts = 'flux' in self.antialias

        # If we are using the sd method
        self.sd = basis.sd

        # Sizes
        self.nupts = basis.nupts
        self.nqpts = basis.nqpts if haveqpts else None
        self.nfpts = basis.nfpts
        self.nfacefpts = basis.nfacefpts
        self.nmpts = basis.nmpts
        self.nfintpts = basis.nfintpts if self.sd else None

    def pri_to_con(pris, cfg):
        pass

    def con_to_pri(cons, cfg):
        pass

    def set_ics_from_cfg(self):
        # Bring simulation constants into scope
        vars = self.cfg.items_as('constants', float)

        if any(d in vars for d in 'xyz'):
            raise ValueError('Invalid constants (x, y, or z) in config file')

        # Get the physical location of each solution point
        coords = self.ploc_at_np('upts').swapaxes(0, 1)
        vars.update(dict(zip('xyz', coords)))

        # Evaluate the ICs from the config file
        ics = [npeval(self.cfg.getexpr('soln-ics', dv), vars)
               for dv in self.privarmap[self.ndims]]

        # Allocate
        self._scal_upts = np.empty((self.nupts, self.nvars, self.neles))

        # Convert from primitive to conservative form
        for i, v in enumerate(self.pri_to_con(ics, self.cfg)):
            self._scal_upts[:, i, :] = v

    def set_ics_from_soln(self, solnmat, solncfg):
        # Recreate the existing solution basis
        solnb = self.basis.__class__(None, solncfg)

        # Form the interpolation operator
        interp = solnb.ubasis.nodal_basis_at(self.basis.upts)

        # Sizes
        nupts, neles, nvars = self.nupts, self.neles, self.nvars

        # Apply and reshape
        self._scal_upts = interp @ solnmat.reshape(solnb.nupts, -1)
        self._scal_upts = self._scal_upts.reshape(nupts, nvars, neles)

    @lazyprop
    def plocfpts(self):
        # Construct the physical location operator matrix
        plocop = self.basis.sbasis.nodal_basis_at(self.basis.fpts)

        # Apply the operator to the mesh elements and reshape
        plocfpts = plocop @ self.eles.reshape(self.nspts, -1)
        plocfpts = plocfpts.reshape(self.nfpts, self.neles, self.ndims)

        return plocfpts

    @lazyprop
    def _srtd_face_fpts(self):
        plocfpts = self.plocfpts.transpose(1, 2, 0)

        return [[np.array(fuzzysort(pts.tolist(), ffpts)) for pts in plocfpts]
                for ffpts in self.basis.facefpts]

    def _scratch_bufs(self):
        pass

    @property
    def _mesh_regions(self):
        off = self._linoff

        # No curved elements
        if off == 0:
            return {'linear': self.neles}
        # All curved elements
        elif off >= self.neles:
            return {'curved': self.neles}
        # Mix of curved and linear elements
        else:
            return {'curved': off, 'linear': self.neles - off}

    def _slice_mat(self, mat, region, ra=None, rb=None):
        off = self._linoff

        # Handle stacked matrices
        if len(mat.ioshape) >= 3:
            off *= mat.ioshape[-2]
        else:
            off = min(off, mat.ncol)

        if region == 'curved':
            return mat.slice(ra, rb, 0, off)
        elif region == 'linear':
            return mat.slice(ra, rb, off, mat.ncol)
        else:
            raise ValueError('Invalid slice region')

    @lazyprop
    def _src_exprs(self):
        convars = self.convarmap[self.ndims]

        # Variable and function substitutions
        subs = self.cfg.items('constants')
        subs.update(x='ploc[0]', y='ploc[1]', z='ploc[2]')
        subs.update({v: f'u[{i}]' for i, v in enumerate(convars)})
        subs.update(abs='fabs', pi=str(math.pi))

        return [self.cfg.getexpr('solver-source-terms', v, '0', subs=subs)
                for v in convars]

    @lazyprop
    def _ploc_in_src_exprs(self):
        return any(re.search(r'\bploc\b', ex) for ex in self._src_exprs)

    @lazyprop
    def _soln_in_src_exprs(self):
        return any(re.search(r'\bu\b', ex) for ex in self._src_exprs)

    def set_backend(self, backend, nscalupts, nonce, linoff):
        self._be = backend

        if self.basis.order >= 2:
            self._linoff = linoff - linoff % -backend.csubsz
        else:
            self._linoff = self.neles

        # Sizes
        ndims, nvars, neles = self.ndims, self.nvars, self.neles
        nfpts, nupts, nqpts = self.nfpts, self.nupts, self.nqpts
        sbufs, abufs = self._scratch_bufs, []

        # Convenience functions for scalar/vector allocation
        alloc = lambda ex, n: abufs.append(
            backend.matrix(n, extent=nonce + ex, tags={'align'})
        ) or abufs[-1]
        salloc = lambda ex, n: alloc(ex, (n, nvars, neles))
        valloc = lambda ex, n: alloc(ex, (ndims, n, nvars, neles))

        # Allocate required scalar scratch space
        if 'scal_fpts' in sbufs and 'scal_qpts' in sbufs:
            self._scal_fqpts = salloc('_scal_fqpts', nfpts + nqpts)
            self._scal_fpts = self._scal_fqpts.slice(0, nfpts)
            self._scal_qpts = self._scal_fqpts.slice(nfpts, nfpts + nqpts)
        elif 'scal_fpts' in sbufs:
            self._scal_fpts = salloc('scal_fpts', nfpts)
        elif 'scal_qpts' in sbufs:
            self._scal_qpts = salloc('scal_qpts', nqpts)

        if 'scal_fintpts' in sbufs:
            self._scal_fintpts = salloc('scal_fintpts', self.nfintpts)

        # Allocate additional scalar scratch space
        if 'scal_upts_cpy' in sbufs:
            self._scal_upts_cpy = salloc('scal_upts_cpy', nupts)

        # Allocate required vector scratch space
        if 'vect_upts' in sbufs:
            self._vect_upts = valloc('vect_upts', nupts)
        if 'vect_qpts' in sbufs:
            self._vect_qpts = valloc('vect_qpts', nqpts)
        if 'vect_fpts' in sbufs:
            self._vect_fpts = valloc('vect_fpts', nfpts)
        if 'vect_fintpts' in sbufs:
            self._vect_fintpts = valloc('vect_fintpts', self.nfintpts)

        # Allocate and bank the storage required by the time integrator
        self._scal_upts = [backend.matrix(self._scal_upts.shape,
                                          self._scal_upts, tags={'align'})
                           for i in range(nscalupts)]
        self.scal_upts_inb = inb = backend.matrix_bank(self._scal_upts)
        self.scal_upts_outb = backend.matrix_bank(self._scal_upts)

        # Find/allocate space for a solution-sized scalar that is
        # allowed to alias other scratch space in the simulation
        aliases = next((m for m in abufs if m.nbytes >= inb.nbytes), None)
        self._scal_upts_temp = backend.matrix(inb.ioshape, aliases=aliases,
                                              tags=inb.tags)

    @memoize
    def opmat(self, expr):
        return self._be.const_matrix(self.basis.opmat(expr),
                                     tags={expr, 'align'})

    def sliceat(fn):
        @memoize
        def newfn(self, name, side=None):
            mat = fn(self, name)

            if side is not None:
                return self._slice_mat(mat, side)
            else:
                return mat

        return newfn

    @memoize
    def smat_at_np(self, name):
        smats_mpts, _ = self._smats_djacs_mpts

        # Interpolation matrix to pts
        m0 = self.basis.mbasis.nodal_basis_at(getattr(self.basis, name))

        # Interpolate the smats
        smats = np.array([m0 @ smat for smat in smats_mpts])
        return smats.reshape(self.ndims, -1, self.ndims, self.neles)

    @sliceat
    @memoize
    def smat_at(self, name):
        return self._be.const_matrix(self.smat_at_np(name), tags={'align'})

    @memoize
    def rcpdjac_at_np(self, name):
        _, djacs_mpts = self._smats_djacs_mpts

        # Interpolation matrix to pts
        pt = getattr(self.basis, name) if isinstance(name, str) else name
        m0 = self.basis.mbasis.nodal_basis_at(pt)

        # Interpolate the djacs
        djac = m0 @ djacs_mpts

        if np.any(djac < -1e-5):
            raise RuntimeError('Negative mesh Jacobians detected')

        return 1.0 / djac

    @sliceat
    @memoize
    def rcpdjac_at(self, name):
        return self._be.const_matrix(self.rcpdjac_at_np(name), tags={'align'})

    @memoize
    def ploc_at_np(self, name):
        pt = getattr(self.basis, name) if isinstance(name, str) else name
        op = self.basis.sbasis.nodal_basis_at(pt)

        ploc = op @ self.eles.reshape(self.nspts, -1)
        ploc = ploc.reshape(-1, self.neles, self.ndims).swapaxes(1, 2)

        return ploc

    @sliceat
    @memoize
    def ploc_at(self, name):
        return self._be.const_matrix(self.ploc_at_np(name), tags={'align'})

    @lazyprop
    def upts(self):
        return self._be.const_matrix(self.basis.upts)

    @lazyprop
    def qpts(self):
        return self._be.const_matrix(self.basis.qpts)

    @lazyprop
    def fintpts(self):
        return self._be.const_matrix(self.basis.fintpts)

    def _gen_pnorm_fpts(self):
        smats = self.smat_at_np('fpts').transpose(1, 3, 0, 2)

        # We need to compute |J|*[(J^{-1})^{T}.N] where J is the
        # Jacobian and N is the normal for each fpt.  Using
        # J^{-1} = S/|J| where S are the smats, we have S^{T}.N.
        pnorm_fpts = np.einsum('ijlk,il->ijk', smats, self.basis.norm_fpts)

        # Compute the magnitudes of these flux point normals
        mag_pnorm_fpts = np.einsum('...i,...i', pnorm_fpts, pnorm_fpts)
        mag_pnorm_fpts = np.sqrt(mag_pnorm_fpts)

        # Check that none of these magnitudes are zero
        if np.any(mag_pnorm_fpts < 1e-10):
            raise RuntimeError('Zero face normals detected')

        # Normalize the physical normals at the flux points
        self._norm_pnorm_fpts = pnorm_fpts / mag_pnorm_fpts[..., None]
        self._mag_pnorm_fpts = mag_pnorm_fpts

    @lazyprop
    def _norm_pnorm_fpts(self):
        self._gen_pnorm_fpts()
        return self._norm_pnorm_fpts

    @lazyprop
    def _mag_pnorm_fpts(self):
        self._gen_pnorm_fpts()
        return self._mag_pnorm_fpts

    @lazyprop
    def _smats_djacs_mpts(self):
        # Metric basis with grid point (q<=p) or pseudo grid points (q>p)
        mpts = self.basis.mpts
        mbasis = self.basis.mbasis

        # Dimensions, number of elements and number of mpts
        ndims, neles, nmpts = self.ndims, self.neles, self.nmpts

        # Physical locations of the pseudo grid points
        x = self.ploc_at_np('mpts')

        # Jacobian operator at these points
        jacop = np.rollaxis(mbasis.jac_nodal_basis_at(mpts), 2)
        jacop = jacop.reshape(-1, nmpts)

        # Cast as a matrix multiply and apply to eles
        jac = jacop @ x.reshape(nmpts, -1)

        # Reshape (nmpts*ndims, neles*ndims) => (nmpts, ndims, neles, ndims)
        jac = jac.reshape(nmpts, ndims, ndims, neles)

        # Transpose to get (ndims, ndims, nmpts, neles)
        jac = jac.transpose(1, 2, 0, 3)

        smats = np.empty((ndims, nmpts, ndims, neles))

        if ndims == 2:
            a, b, c, d = jac[0, 0], jac[1, 0], jac[0, 1], jac[1, 1]

            smats[0, :, 0], smats[0, :, 1] = d, -b
            smats[1, :, 0], smats[1, :, 1] = -c, a

            djacs = a*d - b*c
        else:
            dtt = []
            for dx in jac:
                # Compute x cross x_(chi)
                tt = np.cross(x, dx, axisa=1, axisb=0, axisc=1)

                # Jacobian of x cross x_(chi) at the pseudo grid points
                dt = jacop @ tt.reshape(nmpts, -1)
                dt = dt.reshape(nmpts, ndims, ndims, -1).swapaxes(0, 1)

                dtt.append(dt)

            # Kopriva's invariant form of smats; JSC 26(3), 301-327, Eq. (37)
            smats[0] = 0.5*(dtt[2][1] - dtt[1][2])
            smats[1] = 0.5*(dtt[0][2] - dtt[2][0])
            smats[2] = 0.5*(dtt[1][0] - dtt[0][1])

            # We note that J = [x0; x1; x2]
            x0, x1, x2 = jac

            # Exploit the fact that det(J) = x0 · (x1 ^ x2)
            x1cx2 = np.cross(x1, x2, axisa=0, axisb=0, axisc=1)
            djacs = np.einsum('ij...,ji...->j...', x0, x1cx2)

        return smats.reshape(ndims, nmpts, -1), djacs

    def get_mag_pnorms(self, eidx, fidx):
        fpts_idx = self.basis.facefpts[fidx]
        return self._mag_pnorm_fpts[fpts_idx, eidx]

    def get_mag_pnorms_for_inter(self, eidx, fidx):
        fpts_idx = self._srtd_face_fpts[fidx][eidx]
        return self._mag_pnorm_fpts[fpts_idx, eidx]

    def get_norm_pnorms_for_inter(self, eidx, fidx):
        fpts_idx = self._srtd_face_fpts[fidx][eidx]
        return self._norm_pnorm_fpts[fpts_idx, eidx]

    def get_norm_pnorms(self, eidx, fidx):
        fpts_idx = self.basis.facefpts[fidx]
        return self._norm_pnorm_fpts[fpts_idx, eidx]

    def get_scal_fpts_for_inter(self, eidx, fidx):
        nfp = self.nfacefpts[fidx]

        rmap = self._srtd_face_fpts[fidx][eidx]
        cmap = (eidx,)*nfp

        return (self._scal_fpts.mid,)*nfp, rmap, cmap

    def get_vect_fpts_for_inter(self, eidx, fidx):
        nfp = self.nfacefpts[fidx]

        rmap = self._srtd_face_fpts[fidx][eidx]
        cmap = (eidx,)*nfp
        rstri = (self.nfpts,)*nfp

        return (self._vect_fpts.mid,)*nfp, rmap, cmap, rstri

    def get_ploc_for_inter(self, eidx, fidx):
        fpts_idx = self._srtd_face_fpts[fidx][eidx]
        return self.plocfpts[fpts_idx, eidx]

    def add_sd_kernels(self, name, tplargs, av, folder, *args, **kwargs):
        # Add the specific kernels for the SDM and their elements
        if self.sd is False:
            return

        u = lambda s: self._slice_mat(self._scal_fintpts, s)
        f = lambda s: self._slice_mat(self._vect_fintpts, s)
        pts, npts = 'fintpts', self.nfintpts

        # Mesh regions
        regions = self._mesh_regions

        # Elements with which we use tflux mako
        # Such mako supposes that each unique
        # internal flux point contributes to
        # ndim degrees of freedom
        # This is not optimal for TensorProd nor pri
        eles_without_opt = ['tri', 'tet', 'pri']

        if self.basis.name in eles_without_opt:
            if 'curved' in regions:
                self.kernels[f'{name}_curved'] = lambda: self._be.kernel(
                    'tflux', tplargs=tplargs, dims=[npts, regions['curved']],
                    u=u('curved'), f=f('curved'),
                    artvisc=self._slice_mat(av, 'linear') if av else None,
                    smats=self.smat_at(pts, 'curved')
                )
            if 'linear' in regions:
                upts = getattr(self, pts)
                self.kernels[f'{name}_linear'] = lambda: self._be.kernel(
                    'tfluxlin', tplargs=tplargs, dims=[npts, regions['linear']],
                    u=u('linear'), f=f('linear'),
                    artvisc=self._slice_mat(av, 'linear') if av else None,
                    verts=self.ploc_at('linspts', 'linear'), upts=upts,
                )
        elif self.basis.name in ['quad', 'hex']:
            # Optimize the kernels of tensor product elements
            # by avoiding not needed smats*flux multiplications
            # and reducing the size of the divergence kernel
            self._be.pointwise.register(f'{folder}.tfluxsdrt_tp')
            idxs = np.zeros(self.nfintpts, dtype=np.int)
            nptsint_dim = self.nfintpts/self.ndims
            for i in range(self.nfintpts):
                idxs[i] = int(i/nptsint_dim)

            # Obtain the appropriate rows of smats at internal flux
            # points
            # Recall that at internal flux points only one component
            # of the transformed flux will be used to compute one
            # component of the divergence
            # (if the implementation is optimized of course)
            smats = self.smat_at_np('fintpts')
            smats = np.array([smats[idx, j, :, :] for j, idx in enumerate(idxs)])
            smats_fintpts = self._be.const_matrix(smats, tags={'align'})
            if 'curved' in regions:
                self.kernels[f'{name}_curved'] = lambda: self._be.kernel(
                        'tfluxsdrt_tp', tplargs=tplargs,
                        dims=[self.nfintpts, regions['curved']],
                        u=u('curved'), f=f('curved'),
                        artvisc=self._slice_mat(av, 'curved') if av else None,
                        smats=self._slice_mat(smats_fintpts, 'curved')
                    )
            if 'linear' in regions:
                # Treat the linear regions as curved also
                # The linear kernel performs worse in the tests
                self.kernels[f'{name}_linear'] = lambda: self._be.kernel(
                        'tfluxsdrt_tp', tplargs=tplargs,
                        dims=[self.nfintpts, regions['linear']],
                        u=u('linear'), f=f('linear'),
                        artvisc=self._slice_mat(av, 'linear') if av else None,
                        smats=self._slice_mat(smats_fintpts, 'linear')
                    )

            # Overwrite the divergence kernel, using the optimized
            # matrix for TensorProd elements
            self.kernels['tdivtpcorf'] = lambda: self._be.kernel(
                'mul', self.opmat('M17'), self._scal_fintpts,
                out=self.scal_upts_outb
            )
        else:
            raise RuntimeError('Should not be here')
        return
        if 'curved' in regions:
            self.kernels[f'{name}_curved'] = lambda: self._be.kernel(
                'tflux', tplargs=tplargs, dims=[npts, regions['curved']],
                u=u('curved'), f=f('curved'),
                artvisc=self._slice_mat(av, 'linear') if av else None,
                smats=self.smat_at(pts, 'curved')
            )
            
            nonce = '0'
            abufs = []
            alloc = lambda ex, n: abufs.append(
                self._be.matrix(n, extent=nonce + ex, tags={'align'})
            ) or abufs[-1]
            salloc = lambda ex, n: alloc(ex, (n, self.nvars, self.neles))

            # self._be.pointwise.register(f'pyfr.solvers.navstokes.kernels.tfluxoptsd')
            self._be.pointwise.register(f'pyfr.solvers.linearadvecdiff.kernels.tfluxoptsd')
            self.nintdofs = len(self.basis.fintpts_stack)
            self._scal_fdofs = salloc('_scal_fdofs', self.nintdofs)
            tplargs_cpy = tplargs.copy()
            tplargs_cpy.update({'nintdofs': self.nintdofs, 'nfintpts': self.nfintpts})
            gradxi = self._vect_fintpts.slice(0*npts, 1*npts)
            gradeta = self._vect_fintpts.slice(1*npts, 2*npts)
            gradzeta = self._vect_fintpts.slice(2*npts, 3*npts) if self.basis.ndims == 3 else gradxi

            ele_type_dict = {'quad': 1.0, 'hex': 1.0, 'tri': 2.0, 'tet': 2.0}
            ele_type_dict['pri'] = 3.0
            self.kernels[f'{name}_curved'] = lambda: self._be.kernel(
                'tfluxoptsd', tplargs=tplargs_cpy, dims=[regions['curved']],
                uin=u('curved'),
                gradxi=gradxi, gradeta=gradeta, gradzeta=gradzeta,
                fdof=self._scal_fdofs,
                # artvisc=self._slice_mat(av, 'linear') if av else None,
                smats=self.smat_at(pts, 'curved'),
                t_ele=ele_type_dict[self.basis.name]
            )

        if 'linear' in regions:
            raise RuntimeError('Linear elements not compatible')
            upts = getattr(self, pts)
            self.kernels[f'{name}_linear'] = lambda: self._be.kernel(
                'tfluxlin', tplargs=tplargs, dims=[npts, regions['linear']],
                u=u('linear'), f=f('linear'),
                artvisc=self._slice_mat(av, 'linear') if av else None,
                verts=self.ploc_at('linspts', 'linear'), upts=upts,
            )

        # Overwrite the divergence kernel, using the optimized
        # matrix for TensorProd elements
        self.kernels['tdivtpcorf'] = lambda: self._be.kernel(
            'mul', self.opmat('M17'), self._scal_fdofs,
            out=self.scal_upts_outb
        )
        return

