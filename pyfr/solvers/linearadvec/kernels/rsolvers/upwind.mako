# -*- coding: utf-8 -*-
<%namespace module='pyfr.backends.base.makoutil' name='pyfr'/>
<%include file='pyfr.solvers.linearadvec.kernels.flux'/>

<%pyfr:macro name='rsolve' params='ul, ur, n, nf'>
    // Compute the left and right fluxes + velocities and pressures
    fpdtype_t fl[${ndims}][${nvars}], fr[${ndims}][${nvars}];

    ${pyfr.expand('inviscid_flux', 'ul', 'fl')};
    ${pyfr.expand('inviscid_flux', 'ur', 'fr')};

    fpdtype_t dot = 0;
    % for i in range(ndims):
        dot += n[${i}]*${c['a'][i]};
    % endfor
    // Output
% for i in range(nvars):
    nf[${i}] = ${" + ".join(f"(dot > 0 ? n[{j}]*fl[{j}][{i}]"
    	                    f" : n[{j}]*fr[{j}][{i}])" for j in range(ndims))};
% endfor
</%pyfr:macro>
