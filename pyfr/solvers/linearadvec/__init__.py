# -*- coding: utf-8 -*-

from pyfr.solvers.linearadvec.elements import BaseFluidElements
from pyfr.solvers.linearadvec.inters import LinearAdvecTplargs
from pyfr.solvers.linearadvec.system import LinearAdvecSystem
