# -*- coding: utf-8 -*-

import numpy as np

from pyfr.solvers.baseadvec import (BaseAdvectionIntInters,
                                    BaseAdvectionMPIInters,
                                    BaseAdvectionBCInters)


class LinearAdvecTplargs(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        rsolver = self.cfg.get('solver-interfaces', 'riemann-solver')
        upwind_fact = float(self.cfg.get('solver-interfaces', 'alpha', 1))

        # TODO: use angles to define the advection
        # velocity components
        # This needs to also be added to system.py
        # Therefore, it is not too portable...

        # Calculate adv velocity components from the inflow angle
        # theta = self.c['c-theta']*np.pi/180.0
        # velcomps = np.array([np.cos(theta), np.sin(theta), 1.0])

        # # Adjust adv velocity components for 3-D
        # if self.ndims == 3:
        #     phi = self.c['c-phi']*np.pi/180.0
        #     velcomps[:2] *= np.sin(phi)
        #     velcomps[2] *= np.cos(phi)

        # self.c['a'] = self.c['c']*velcomps[:self.ndims]

        self._tplargs = dict(ndims=self.ndims, nvars=self.nvars,
                             rsolver=rsolver, c=self.c, alpha=upwind_fact)


class LinearAdvecIntInters(LinearAdvecTplargs, BaseAdvectionIntInters):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._be.pointwise.register('pyfr.solvers.linearadvec.kernels.intcflux')

        self.kernels['comm_flux'] = lambda: self._be.kernel(
            'intcflux', tplargs=self._tplargs, dims=[self.ninterfpts],
            ul=self._scal_lhs, ur=self._scal_rhs,
            magnl=self._mag_pnorm_lhs, nl=self._norm_pnorm_lhs
        )


class LinearAdvecMPIInters(LinearAdvecTplargs, BaseAdvectionMPIInters):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._be.pointwise.register('pyfr.solvers.linearadvec.kernels.mpicflux')

        rsolver = self.cfg.get('solver-interfaces', 'riemann-solver')
        tplargs = dict(ndims=self.ndims, nvars=self.nvars, rsolver=rsolver,
                       c=self.c)

        self.kernels['comm_flux'] = lambda: self._be.kernel(
            'mpicflux', tplargs, dims=[self.ninterfpts],
            ul=self._scal_lhs, ur=self._scal_rhs,
            magnl=self._mag_pnorm_lhs, nl=self._norm_pnorm_lhs
        )


class LinearAdvecBaseBCInters(LinearAdvecTplargs, BaseAdvectionBCInters):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._be.pointwise.register('pyfr.solvers.linearadvec.kernels.bccflux')

        # Additional BC specific template arguments
        self._tplargs['bctype'] = self.type

        self.kernels['comm_flux'] = lambda: self._be.kernel(
            'bccflux', tplargs=self._tplargs, dims=[self.ninterfpts],
            extrns=self._external_args, ul=self._scal_lhs,
            magnl=self._mag_pnorm_lhs, nl=self._norm_pnorm_lhs,
            **self._external_vals
        )


class LinearAdvecInflowBCInters(LinearAdvecBaseBCInters):
    type = 'in'

    def __init__(self, be, lhs, elemap, cfgsect, cfg):
        super().__init__(be, lhs, elemap, cfgsect, cfg)

        tplc = self._exp_opts(
            ['phi'][:1], lhs
        )
        self.c.update(tplc)


class LinearAdvecOutflow(LinearAdvecBaseBCInters):
    type = 'out'
    cflux_state = 'ghost'
