# -*- coding: utf-8 -*-

from pyfr.solvers.baseadvecdiff import BaseAdvectionDiffusionSystem
from pyfr.solvers.sdnavstokes.elements import NavierStokesElementsSD
from pyfr.solvers.navstokes.inters import (NavierStokesBaseBCInters,
                                           NavierStokesIntInters,
                                           NavierStokesMPIInters)


class NavierStokesSystemSD(BaseAdvectionDiffusionSystem):
    name = 'sd-navier-stokes'

    elementscls = NavierStokesElementsSD
    intinterscls = NavierStokesIntInters
    mpiinterscls = NavierStokesMPIInters
    bbcinterscls = NavierStokesBaseBCInters

    def __init__(self, backend, rallocs, mesh, initsoln, nregs, cfg):
        super().__init__(backend, rallocs, mesh, initsoln, nregs, cfg)

        self.use_corr_grads = (False
                               if cfg.hasopt('solver', 'use_no_corr_grads')
                               else True)

        print(f'{self.use_corr_grads}')

    def rhs(self, t, uinbank, foutbank):
        runall = self.backend.runall
        q1, q2 = self._queues
        kernels = self._kernels

        self._bc_inters.prepare(t)

        self.eles_scal_upts_inb.active = uinbank
        self.eles_scal_upts_outb.active = foutbank

        # Interpolate to external flux points
        # In: scal_upts_inb
        # Out: _scal_fpts
        q1.enqueue(kernels['eles', 'disu'])
        # Interpolate to internal flux points
        # In: scal_upts_inb
        # Out: _scal_fintpts
        q1.enqueue(kernels['eles', 'disuint'])
        q1.enqueue(kernels['mpiint', 'scal_fpts_pack'])
        runall([q1])

        if ('eles', 'copy_soln') in kernels:
            # If source terms
            q1.enqueue(kernels['eles', 'copy_soln'])
        if ('iint', 'copy_fpts') in kernels:
            # If \beta = 0.5 in LDG gradient scheme
            q1.enqueue(kernels['iint', 'copy_fpts'])
        q1.enqueue(kernels['iint', 'con_u'])
        q1.enqueue(kernels['bcint', 'con_u'], t=t)
        if ('eles', 'shocksensor') in kernels:
            q1.enqueue(kernels['eles', 'shocksensor'])
            q1.enqueue(kernels['mpiint', 'artvisc_fpts_pack'])

        if self.use_corr_grads:
            # Compute contribution of internal flux points to the corrected
            # gradient
            # In: _scal_fintpts
            # Out: _vect_upts
            q1.enqueue(kernels['eles', 'tgradpcorufint_upts'])
        q2.enqueue(kernels['mpiint', 'scal_fpts_send'])
        q2.enqueue(kernels['mpiint', 'scal_fpts_recv'])
        q2.enqueue(kernels['mpiint', 'scal_fpts_unpack'])

        runall([q1, q2])

        q1.enqueue(kernels['mpiint', 'con_u'])
        if self.use_corr_grads:
            # Compute contribution of external flux points to the corrected
            # gradient at solution points
            # In: _vect_fpts.slice(0, self.nfpts)
            # Out: _vect_upts with beta=1.0
            q1.enqueue(kernels['eles', 'tgradcorufext_upts'])
        else:
            # Compute uncorrected gradients at solution points
            # In: scal_upts_inb
            # Out: _vect_upts
            q1.enqueue(kernels['eles', 'tgradpu_upts'])

        # Compute transformed gradients at solution points
        # In: _vect_upts
        # Out: _vect_upts
        q1.enqueue(kernels['eles', 'gradcoru_upts_curved'])
        q1.enqueue(kernels['eles', 'gradcoru_upts_linear'])

        # Interpolate gradients to external flux points
        # In: _vect_upts
        # Out: _vect_fpts
        q1.enqueue(kernels['eles', 'gradcoru_fpts'])

        # Interpolate gradients to internal flux points
        # In: _vect_upts
        # Out: _vect_fintpts
        q1.enqueue(kernels['eles', 'gradcoru_fintpts'])

        # Pack gradient data
        q1.enqueue(kernels['mpiint', 'vect_fpts_pack'])
        if ('eles', 'shockvar') in kernels:
            q2.enqueue(kernels['mpiint', 'artvisc_fpts_send'])
            q2.enqueue(kernels['mpiint', 'artvisc_fpts_recv'])
            q2.enqueue(kernels['mpiint', 'artvisc_fpts_unpack'])

        runall([q1, q2])

        if ('eles', 'gradcoru_qpts') in kernels:
            q1.enqueue(kernels['eles', 'gradcoru_qpts'])

        # Compute transformed fluxes at internal flux points
        # If non TensorProduct then
        # In: _vect_fintpts
        # Out: _vect_fintpts
        # Else
        # In: _vect_fintpts
        # Out: _scal_fintpts
        q1.enqueue(kernels['eles', 'tdisfint_curved'])
        q1.enqueue(kernels['eles', 'tdisfint_linear'])

        # Compute contribution of internal flux points to the divergence
        # If non TensorProduct then
        # In: vect_fintpts
        # Out: scal_upts_outb
        # Else
        # In: scal_fintpts
        # Out: scal_upts_outb
        q1.enqueue(kernels['eles', 'tdivtpcorfint'])

        # Compute common fluxes at internal interfaces
        q1.enqueue(kernels['iint', 'comm_flux'])

        # Compute common fluxes at boundaries
        q1.enqueue(kernels['bcint', 'comm_flux'], t=t)

        # Send/Recv MPI gradient data
        q2.enqueue(kernels['mpiint', 'vect_fpts_send'])
        q2.enqueue(kernels['mpiint', 'vect_fpts_recv'])
        q2.enqueue(kernels['mpiint', 'vect_fpts_unpack'])

        runall([q1, q2])

        # Compute common fluxes at MPI interfaces
        q1.enqueue(kernels['mpiint', 'comm_flux'])

        # Compute contribution of external flux points to the divergence
        # In: vect_fintpts_inb
        # Out: scal_upts_outb
        q1.enqueue(kernels['eles', 'tdivtconfext'])
        # Update divergence
        # In: _scal_fpts
        # Out: scal_upts_outb with beta=1.0
        q1.enqueue(kernels['eles', 'negdivconf'], t=t)
        runall([q1])

    def compute_grads(self, t, uinbank):
        runall = self.backend.runall
        q1, q2 = self._queues
        kernels = self._kernels

        self._bc_inters.prepare(t)

        self.eles_scal_upts_inb.active = uinbank

        if self.use_corr_grads:
            # Compute corrected grads to LDG approach
            q1.enqueue(kernels['eles', 'disu'])
            q1.enqueue(kernels['eles', 'disuint'])
            q1.enqueue(kernels['mpiint', 'scal_fpts_pack'])
            runall([q1])

            if ('iint', 'copy_fpts') in kernels:
                q1.enqueue(kernels['iint', 'copy_fpts'])
            q1.enqueue(kernels['iint', 'con_u'])
            q1.enqueue(kernels['bcint', 'con_u'], t=t)
            q1.enqueue(kernels['eles', 'tgradpcorufint_upts'])
            q2.enqueue(kernels['mpiint', 'scal_fpts_send'])
            q2.enqueue(kernels['mpiint', 'scal_fpts_recv'])
            q2.enqueue(kernels['mpiint', 'scal_fpts_unpack'])

            runall([q1, q2])

            q1.enqueue(kernels['mpiint', 'con_u'])
            q1.enqueue(kernels['eles', 'tgradcorufext_upts'])
        else:
            # Compute uncorrected gradients at solution points
            # In: scal_upts_inb
            # Out: _vect_upts
            q1.enqueue(kernels['eles', 'tgradpu_upts'])

        # Transform gradients
        q1.enqueue(kernels['eles', 'gradcoru_upts_curved'])
        q1.enqueue(kernels['eles', 'gradcoru_upts_linear'])

        runall([q1])
