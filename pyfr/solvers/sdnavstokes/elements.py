# -*- coding: utf-8 -*-

import numpy as np

from pyfr.backends.base.kernels import ComputeMetaKernel, NullComputeKernel
from pyfr.solvers.navstokes.elements import NavierStokesElements
from pyfr.util import lazyprop
from pyfr.shapes import TensorProdShape, TriShape


class NavierStokesElementsSD(NavierStokesElements):
    # Use the density field for shock sensing
    shockvar = 'rho'

    def __init__(self, basiscls, eles, cfg):
        super().__init__(basiscls, eles, cfg)
        self.nfintpts = self.basis.nfintpts

    def set_backend(self, backend, nscalupts, nonce, linoff):
        super().set_backend(backend, nscalupts, nonce, linoff)

        sbufs, abufs = self._scratch_bufs, []
        # Convenience functions for scalar/vector allocation
        alloc = lambda ex, n: abufs.append(
            self._be.matrix(n, extent=nonce + ex, tags={'align'})
        ) or abufs[-1]
        salloc = lambda ex, n: alloc(ex, (n, self.nvars, self.neles))
        valloc = lambda ex, n: alloc(ex, (self.ndims, n, self.nvars, self.neles))

        # This backend kernels operations may be different
        # for each type of element. Therefore, assess
        # if the elements are tensor-product like cells
        # to decide whether to use SD or FR formulation
        use_sd = issubclass(type(self.basis), TensorProdShape)
        use_sd = use_sd | issubclass(type(self.basis), TriShape)

        # With SD formulation, extra allocations are needed
        # for data defined at internal flux points
        if use_sd:
            # To store interpolated conservative variables,
            # and transformed fluxes at internal flux points
            self._scal_fintpts = salloc('scal_fintpts', self.nfintpts)
            # To store interpolated gradients at internal flux points
            # and transformed fluxes (in the less performant method)
            # at internal flux points
            self._vect_fintpts = valloc('vect_fintpts', self.nfintpts)

        # Handle shock capturing and Sutherland's law
        shock_capturing = self.cfg.get('solver', 'shock-capturing')
        visc_corr = self.cfg.get('solver', 'viscosity-correction', 'none')
        if visc_corr not in {'sutherland', 'none'}:
            raise ValueError('Invalid viscosity-correction option')

        # Template parameters for the flux kernels
        tplargs = {
            'ndims': self.ndims,
            'nvars': self.nvars,
            'nfintpts': self.nfintpts,
            'nverts': len(self.basis.linspts),
            'c': self.cfg.items_as('constants', float),
            'jac_exprs': self.basis.jac_exprs,
            'shock_capturing': shock_capturing,
            'visc_corr': visc_corr
        }

        av = self.artvisc

        kernels = self.kernels
        kernel = self._be.kernel

        # Interpolation to internal flux points
        if use_sd:
            kernels['disuint'] = lambda: self._be.kernel(
                'mul', self.opmat('M12'), self.scal_upts_inb,
                out=self._scal_fintpts
            )
        else:
            # If not using SD, no need to interpolate to internal
            # flux points
            kernels['disuint'] = lambda: NullComputeKernel()

        # Computation of the gradients without correction
        self.kernels['tgradpu_upts'] = lambda: kernel(
            'mul', self.opmat('M4'), self.scal_upts_inb,
            out=self._vect_upts
        )

        # Contribution of internal flux points to corrected gradients at
        # solution points
        if use_sd:
            self.kernels['tgradpcorufint_upts'] = lambda: kernel(
                'mul', self.opmat('M15'), self._scal_fintpts,
                out=self._vect_upts
            )
        else:
            # If not using SD, utilize FR method to compute
            # the contribution of solution point data to the gradient
            # at solution points
            self.kernels['tgradpcorufint_upts'] = self.kernels['tgradpcoru_upts']

        # Contribution of external flux points to corrected gradients at
        # solution points
        if use_sd:
            self.kernels['tgradcorufext_upts'] = lambda: kernel(
                'mul', self.opmat('M16'), self._vect_fpts.slice(0, self.nfpts),
                out=self._vect_upts, beta=1.0
            )
        else:
            # If not using SD, use the FR equivalent
            self.kernels['tgradcorufext_upts'] = self.kernels['tgradcoru_upts']

        # Interpolation of gradients to internal flux points
        def gradcoru_fintpts():
            nupts, nfpts = self.nupts, self.nfintpts
            vupts, vfpts = self._vect_upts, self._vect_fintpts

            # Exploit the block-diagonal form of the operator
            muls = [kernel('mul', self.opmat('M12'),
                           vupts.slice(i*nupts, (i + 1)*nupts),
                           vfpts.slice(i*nfpts, (i + 1)*nfpts))
                    for i in range(self.ndims)]

            return ComputeMetaKernel(muls)

        if use_sd:
            kernels['gradcoru_fintpts'] = gradcoru_fintpts
        else:
            # If not using SD, then there is no need to interpolate
            # the gradients
            kernels['gradcoru_fintpts'] = lambda: NullComputeKernel()

        # Divergence contribution of internal flux points
        if use_sd:
            if issubclass(type(self.basis), TensorProdShape):
                # Supposing that there is a unique components of
                # the transformed flux at each internal flux points
                kernels['tdivtpcorfint'] = lambda: self._be.kernel(
                    'mul', self.opmat('M17'), self._scal_fintpts,
                    out=self.scal_upts_outb
                )
            else:
                # Supposing that there are three components of
                # the transformed flux at each internal flux points
                kernels['tdivtpcorfint'] = lambda: self._be.kernel(
                    'mul', self.opmat('M13'), self._vect_fintpts,
                    out=self.scal_upts_outb
                )
        else:
            # If not using SD, compute the FR contribution of
            # solution points to solution points
            kernels['tdivtpcorfint'] = kernels['tdivtpcorf']

        # Contribution of external flux points to divergence at solution points
        if use_sd:
            kernels['tdivtconfext'] = lambda: self._be.kernel(
                'mul', self.opmat('M14'), self._scal_fpts,
                out=self.scal_upts_outb, beta=1.0
            )
        else:
            # If not using SD, compute the FR contribution of
            # external flux points to solution points
            kernels['tdivtconfext'] = kernels['tdivtconf']

        # Mesh regions
        regions = self._mesh_regions

        if use_sd:
            uint = lambda s: self._slice_mat(self._scal_fintpts, s)
            fint = lambda s: self._slice_mat(self._vect_fintpts, s)
            ptsint, nptsint = 'fintpts', self.nfintpts

            # Compute transformed fluxes at internal flux points
            if 'curved' in regions:
                if issubclass(type(self.basis), TensorProdShape):
                    # Supposing that there is a unique components of
                    # the transformed flux at each internal flux points

                    # Generate the indices which represent the derivative
                    # which is computed using each internal flux point
                    idxs = np.zeros(self.nfintpts, dtype=np.int)
                    nptsint_dim = nptsint/self.ndims
                    for i in range(self.nfintpts):
                        idxs[i] = int(i/nptsint_dim)

                    # Obtain the appropriate rows of smats at internal flux
                    # points
                    # Recall that at internal flux points only one component
                    # of the transformed flux will be used to compute one
                    # component of the divergence
                    # (if the implementation is optimized of course)
                    smats = self.smat_at_np(ptsint)
                    smats = np.array([smats[idx, j, :, :]
                                      for j, idx in enumerate(idxs)])
                    smats_fintpts = self._be.const_matrix(smats,
                                                          tags={'align'})

                    self._be.pointwise.register('pyfr.solvers.sdnavstokes.'
                                                'kernels.tfluxsd')
                    self.kernels['tdisfint_curved'] = lambda: self._be.kernel(
                        'tfluxsd', tplargs=tplargs,
                        dims=[nptsint, regions['curved']],
                        u=uint('curved'), f=fint('curved'),
                        artvisc=self._slice_mat(av, 'curved') if av else None,
                        smats=self._slice_mat(smats_fintpts, 'curved')
                    )
                else:
                    # Supposing that there are three components of
                    # the transformed flux at each internal flux points
                    self.kernels['tdisfint_curved'] = lambda: self._be.kernel(
                        'tflux', tplargs=tplargs,
                        dims=[nptsint, regions['curved']],
                        u=uint('curved'), f=fint('curved'),
                        artvisc=self._slice_mat(av, 'curved') if av else None,
                        smats=self.smat_at(ptsint, 'curved')
                    )

            if 'linear' in regions:
                if issubclass(type(self.basis), TensorProdShape):
                    # Supposing that there is a unique components of
                    # the transformed flux at each internal flux points
                    # WARNING: always use the curved formulation
                    self.kernels['tdisfint_linear'] = lambda: self._be.kernel(
                        'tfluxsd', tplargs=tplargs,
                        dims=[nptsint, regions['linear']],
                        u=uint('linear'), f=fint('linear'),
                        artvisc=self._slice_mat(av, 'linear') if av else None,
                        smats=self._slice_mat(smats_fintpts, 'linear')
                    )
                else:
                    # Supposing that there are three components of
                    # the transformed flux at each internal flux points
                    uptsint = getattr(self, ptsint)
                    self.kernels['tdisfint_linear'] = lambda: self._be.kernel(
                        'tfluxlin', tplargs=tplargs,
                        dims=[nptsint, regions['linear']],
                        u=uint('linear'), f=fint('linear'),
                        artvisc=self._slice_mat(av, 'linear') if av else None,
                        verts=self.ploc_at('linspts', 'linear'), upts=uptsint
                    )
        else:
            # Use the FR equivalent, computing transformed fluxes
            # at solution points
            if 'curved' in regions:
                self.kernels['tdisfint_curved'] = self.kernels['tdisf_curved']
            if 'linear' in regions:
                self.kernels['tdisfint_linear'] = self.kernels['tdisf_linear']

    @lazyprop
    def fintpts(self):
        return self._be.const_matrix(self.basis.fintpts)
