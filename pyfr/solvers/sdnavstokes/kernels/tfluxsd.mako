# -*- coding: utf-8 -*-
<%inherit file='base'/>
<%namespace module='pyfr.backends.base.makoutil' name='pyfr'/>

<%include file='pyfr.solvers.baseadvecdiff.kernels.artvisc'/>
<%include file='pyfr.solvers.euler.kernels.flux'/>
<%include file='pyfr.solvers.navstokes.kernels.flux'/>

<%pyfr:kernel name='tfluxsd' ndim='2'
              u='inout fpdtype_t[${str(nvars)}]'
              artvisc='in broadcast-col fpdtype_t'
              f='in fpdtype_t[${str(ndims)}][${str(nvars)}]'
              smats='in fpdtype_t[${str(ndims)}]'>
    // Compute the flux (F = Fi + Fv)
    fpdtype_t ftemp[${ndims}][${nvars}];
    fpdtype_t p, v[${ndims}];
    ${pyfr.expand('inviscid_flux', 'u', 'ftemp', 'p', 'v')};
    ${pyfr.expand('viscous_flux_add', 'u', 'f', 'ftemp')};
    ${pyfr.expand('artificial_viscosity_add', 'f', 'ftemp', 'artvisc')};

    // Transform the fluxes
    // Store only one component of the transformed fluxes
% for j in range(nvars):
    u[${j}] = ${' + '.join(f'smats[{k}]*ftemp[{k}][{j}]'
                              for k in range(ndims))};
% endfor
</%pyfr:kernel>
