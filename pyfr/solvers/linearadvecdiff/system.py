# -*- coding: utf-8 -*-

from pyfr.solvers.baseadvecdiff import BaseAdvectionDiffusionSystem
from pyfr.solvers.linearadvecdiff.elements import LinearAdvecDiffElements
from pyfr.solvers.linearadvecdiff.inters import (LinearAdvecDiffIntInters,
                                                 LinearAdvecDiffMPIInters,
                                                 LinearAdvecDiffBaseBCInters)


class LinearAdvecDiffSystem(BaseAdvectionDiffusionSystem):
    name = 'linearadvecdiff'

    elementscls = LinearAdvecDiffElements
    intinterscls = LinearAdvecDiffIntInters
    mpiinterscls = LinearAdvecDiffMPIInters
    bbcinterscls = LinearAdvecDiffBaseBCInters
