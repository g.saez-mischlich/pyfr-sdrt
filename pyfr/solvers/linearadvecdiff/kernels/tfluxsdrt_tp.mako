# -*- coding: utf-8 -*-
<%inherit file='base'/>
<%namespace module='pyfr.backends.base.makoutil' name='pyfr'/>
<%include file='pyfr.solvers.linearadvec.kernels.flux'/>
<%include file='pyfr.solvers.linearadvecdiff.kernels.flux'/>

<%pyfr:kernel name='tfluxsdrt_tp' ndim='2'
              u='inout fpdtype_t[${str(nvars)}]'
              f='in fpdtype_t[${str(ndims)}][${str(nvars)}]'
              smats='in fpdtype_t[${str(ndims)}]'>
    // Compute the flux
    fpdtype_t ftemp[${ndims}][${nvars}];
    ${pyfr.expand('inviscid_flux', 'u', 'ftemp')};
    ${pyfr.expand('viscous_flux_add', 'f', 'ftemp')};

    // Transform the fluxes
    // Store only one component of the transformed fluxes
% for j in range(nvars):
    u[${j}] = ${' + '.join(f'smats[{k}]*ftemp[{k}][{j}]'
                              for k in range(ndims))};
% endfor
</%pyfr:kernel>
