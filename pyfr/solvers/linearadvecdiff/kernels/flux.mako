# -*- coding: utf-8 -*-
<%namespace module='pyfr.backends.base.makoutil' name='pyfr'/>

<%pyfr:macro name='viscous_flux_add' params='grad_uin, fout'>

% for i in range(ndims):
    fout[${i}][0] -= ${c['mu']}*grad_uin[${i}][0];
% endfor

</%pyfr:macro>