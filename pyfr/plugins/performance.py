# -*- coding: utf-8 -*-

import os
import time

from pyfr.inifile import Inifile
from pyfr.mpiutil import get_comm_rank_root, get_mpi
from pyfr.plugins.base import BasePlugin, init_csv


class PerformancePlugin(BasePlugin):
    name = 'performance'
    systems = ['*']
    formulations = ['std']

    def __init__(self, intg, cfgsect, prefix):
        super().__init__(intg, cfgsect, prefix)

        self.nsteps = self.cfg.getint(self.cfgsect, 'nsteps', 20)

        self.count = 0
        self.stats = []
        self.tprev = intg.tcurr

        self._gndofs = intg._gndofs

        # MPI info
        comm, rank, root = get_comm_rank_root()

        self._nranks = comm.size

        self._nthreads = int(os.environ["OMP_NUM_THREADS"]) if 'OMP_NUM_THREADS' in os.environ else 1

        self._nworkunits = self._nranks*self._nthreads

        self._nworkunits = self._nranks

        self._nstages = None

        self._wtime = time.time()
        # The root rank needs to open the output file
        if rank == root:
            self.outf = init_csv(self.cfg, cfgsect,
                                 'n,elapsed,t,dt,timeperdof,timeperelement')
        else:
            self.outf = None

    def __call__(self, intg):
        if intg.nacptsteps % self.nsteps != 0:
            return

        currtime = time.time()
        elapsed = currtime - self._wtime
        self._wtime = currtime

        if self._nstages is None:
            stats = Inifile()
            intg.collect_stats(stats)
            nfevals = stats.getint('solver-time-integrator', 'nfevals')
            # intg.nsteps takes into account accepted and rejected RK steps
            # as well as nfevals
            self._nstages = nfevals / intg.nsteps

        timeperdof = elapsed*self._nworkunits
        timeperdof /= (self._gndofs*self._nstages*self.nsteps)
        self.stats.append((intg.nacptsteps, elapsed, intg.tcurr,
                           intg.stepinfo[0][0], timeperdof, 0))

        # If we're the root rank then output
        if self.outf:
            for s in self.stats:
                print(*s, sep=',', file=self.outf)

            # Periodically flush to disk
            if intg.nacptsteps % self.nsteps == 0:
                self.outf.flush()
            self.stats = []

        # Reset the stats
        self.stats = []
