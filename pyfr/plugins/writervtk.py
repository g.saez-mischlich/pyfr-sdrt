# -*- coding: utf-8 -*-

from collections import defaultdict

import os
import numpy as np

from pyfr.plugins.base import BasePlugin, PostactionMixin, RegionMixin
from pyfr.mpiutil import get_comm_rank_root
from pyfr.writers.vtk import VTKWriter


class WriterVTKPlugin(PostactionMixin, RegionMixin, BasePlugin):
    name = 'writervtk'
    systems = ['*']
    formulations = ['dual', 'std']

    def __init__(self, intg, cfgsect, suffix=None):
        super().__init__(intg, cfgsect, suffix)

        # Construct the solution writer
        self._writer = self._init_writer_for_region(intg, self.nvars, 'soln')

        # Base output directory and file name
        self.basedir_rel = self.cfg.getpath(self.cfgsect, 'basedir', '.',
                                            abs=False)
        self.basedir = self.cfg.getpath(self.cfgsect, 'basedir', '.',
                                        abs=True)
        self.basename = self.cfg.get(self.cfgsect, 'basename')

        self.nout = 0

        # Divisor for the VTK writer
        order = self.cfg.getint('solver', 'order')
        divisor = self.cfg.getint(self.cfgsect, 'divisor', order)

        # Fill in mesh data for the VTK writer
        rank = intg.rallocs.prank

        # Identify the type of elements in this rank
        enames = [name
                  for name in intg.system.mesh.array_info('spt').keys()
                  if f'_p{rank}' in name]

        self.vtk_mesh_inf = {}
        self.vtk_mesh = {}
        self.vtk_soln = {}
        self.vtk_soln_inf = {}
        for i, (name, ele_region) in enumerate(zip(enames, self._ele_regions)):
            # Element type
            ele_type = name.split('_')[1]

            # Shape of solution and mesh data
            shape = intg.soln[i][..., ele_region[1]].shape

            # Store mesh data
            self.vtk_mesh[name] = intg.system.mesh[name][:, ele_region[1], :]
            self.vtk_mesh_inf[name] = (ele_type, shape)

            # Store solution data
            self.vtk_soln_inf[f'soln_{ele_type}_p{rank}'] = (ele_type, shape)
            self.vtk_soln[f'soln_{ele_type}_p{rank}'] = []

        if self._ele_regions:
            vtk = WriterVTKPlugin.VTKRuntimeWriter
            self._vtkwriter = vtk(intg, self.vtk_mesh, self.vtk_mesh_inf,
                                  self.vtk_soln_inf, divisor)
            self._vtkwriter.soln = self.vtk_soln

        # MPI info
        # Assess which processors are writing this vtu
        comm, rank, root = get_comm_rank_root()
        hasdata = comm.gather(1 if self._ele_regions else 0, root=root)
        if rank == root:
            self.out_ranks = np.where(np.array(hasdata) == 1)[0]

        # Output time step and last output time
        self.dt_out = self.cfg.getfloat(cfgsect, 'dt-out')
        self.tout_last = intg.tcurr

        # Output field names
        self.fields = intg.system.elementscls.convarmap[self.ndims]

        # Register our output times with the integrator
        intg.call_plugin_dt(self.dt_out)

        # If we're not restarting then write out the initial solution
        if not intg.isrestart:
            self.tout_last -= self.dt_out
            self(intg)

    def _write_pvtu(self):
        pfn = os.path.join(self.basedir, self.fname) + '.pvtu'

        def write_s_to_fh(s): return fh.write(s.encode())

        with open(pfn, 'wb') as fh:
            names, types, comps = self._vtkwriter._get_array_attrs()

            write_s_to_fh('<?xml version="1.0" ?>\n<VTKFile '
                          'byte_order="LittleEndian" '
                          'type="PUnstructuredGrid" '
                          f'version="{self._vtkwriter.vtkfile_version}">\n'
                          '<PUnstructuredGrid>\n')

            def write_s(s): return fh.write(s.encode())
            write_s('<PPoints>\n')

            # Write vtk DaraArray headers
            for i, (n, t, s) in enumerate(zip(names, types, comps)):
                pname = self._vtkwriter._process_name(n)
                write_s(f'<PDataArray Name="{pname}" type="{t}" '
                        f'NumberOfComponents="{s}"/>\n')

                if i == 0:
                    write_s('</PPoints>\n<PCells>\n')
                elif i == 3:
                    write_s('</PCells>\n<PPointData>\n')

            write_s('</PPointData>\n')

            # Constitutent pieces
            for rank in self.out_ranks:
                bname = f'{self.fname}.r{rank}.vtu'
                write_s_to_fh(f'<Piece Source="{bname}"/>\n')

            write_s_to_fh('</PUnstructuredGrid>\n</VTKFile>\n')

    def __call__(self, intg):
        if intg.tcurr - self.tout_last < self.dt_out - self.tol:
            return

        # Extract and subset the solution
        soln = [intg.soln[i][..., rgn] for i, rgn in self._ele_regions]

        # Add in any required region data
        data = self._add_region_data(soln)

        for key, sol_ele in zip(self.vtk_soln.keys(), data):
            self.vtk_soln[key] = sol_ele

        # Write out the file
        if self._ele_regions:
            # Substitute {t} and {n} for the current time and output number
            self.fname = self.basename.format(t=intg.tcurr, n=self.nout)
            self._vtkwriter.outf = (os.path.join(self.basedir, self.fname) +
                                    f'.r{intg.rallocs.prank}.vtu')
            self._vtkwriter.tcurr = intg.tcurr
            self._vtkwriter.write_out()

        # Write out pvtu file
        comm, rank, root = get_comm_rank_root()
        if rank == root:
            self._write_pvtu()

        # If a post-action has been registered then invoke it
        self._invoke_postaction(intg=intg, mesh=intg.system.mesh.fname,
                                soln=self._vtkwriter.outf, t=intg.tcurr)

        # Update the last output time
        self.tout_last = intg.tcurr

        self.nout += 1

    class VTKRuntimeWriter(VTKWriter):
        def __init__(self, intg, mesh, mesh_inf, soln_inf, divisor):
            # Load the mesh and solution files
            self.mesh = mesh

            # Load the configuration and stats files
            self.cfg = intg.cfg

            # Data file prefix
            self.dataprefix = 'soln'

            # Get element types and array shapes
            self.mesh_inf = mesh_inf
            self.soln_inf = soln_inf

            # System and elements classes
            self.systemscls = intg.system
            self.elementscls = self.systemscls.elementscls

            # Dimensions
            self.ndims = self.systemscls.ndims
            self.nvars = next(iter(self.soln_inf.values()))[1][1]

            # By default write in single precision
            self.dtype = np.float32
            self.ho_output = True
            self.divisor = divisor
            self.vtkfile_version = '2.1'

            # Divisor for each type element
            self.etypes_div = defaultdict(lambda: self.divisor)
            self.etypes_div['pyr'] += 2

            self._get_npts_ncells_nnodes = self._get_npts_ncells_nnodes_ho

            # Solutions need a separate processing pipeline to other data
            self._pre_proc_fields = self._pre_proc_fields_soln
            self._post_proc_fields = self._post_proc_fields_soln
            self._soln_fields = list(self.elementscls.privarmap[self.ndims])
            self._vtk_vars = list(self.elementscls.visvarmap[self.ndims])
