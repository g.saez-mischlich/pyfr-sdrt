# -*- coding: utf-8 -*-

from collections import Iterable
import numpy as np
from math import sqrt, gamma, factorial

from pyfr.nputil import clean
from pyfr.polys import jacobi, jacobi_diff
from pyfr.util import lazyprop, subclass_where


def get_rt_polybasis(name, order, ptsint, ptsext, dofsint, dofsext):
    return subclass_where(BaseRTPolyBasis, name=name)(order, ptsint,
                                                      ptsext, dofsint,
                                                      dofsext)


def jacobi_norm(n, a, b, z):
    # See Section 18.3 of NIST Handbook of Mathematical Functions
    # https://dlmf.nist.gov/18.3
    jac = jacobi(n, a, b, z)
    return [j/sqrt(2**(a + b + 1)*gamma(a + n + 1)*gamma(b + n + 1)/((a + b + 2*n + 1)*factorial(n)*gamma(a + b + n + 1))) for n, j in enumerate(jac)]


def jacobi_diff_norm(n, a, b, z):
    jac = jacobi_diff(n, a, b, z)
    return [j/sqrt(2**(a + b + 1)*gamma(a + n + 1)*gamma(b + n + 1)/((a + b + 2*n + 1)*factorial(n)*gamma(a + b + n + 1))) for n, j in enumerate(jac)]


def jacobian(f, x):
    # h = mpmath.sqrt(mpmath.eps)
    h = sqrt(1e-16)
    h2 = h / 2

    J = []
    for j in range(len(x)):
        xph2 = [xk if k != j else xk + h2 for k, xk in enumerate(x)]
        xmh2 = [xk if k != j else xk - h2 for k, xk in enumerate(x)]

        fxph2 = f(*xph2)
        fxmh2 = f(*xmh2)

        J.append([(fp[j] - fm[j]) / h for fp, fm in zip(fxph2, fxmh2)])

    J = np.array(J).sum(axis=0)
    return J


class BaseRTPolyBasis(object):
    name = None

    def __init__(self, order, ptsint, ptsext, dofsint, dofsext):
        self.order = order
        self.ptsint = ptsint
        self.pts = np.vstack([ptsint, ptsext])
        self.dofs = np.vstack([dofsint, dofsext])
        self.dofsint = dofsint
        self.dofsext = dofsext
        assert(len(self.pts) == len(self.dofs))
        assert(len(self.pts) == len(self.monobasis))
        # self.test_vdm()

    @lazyprop
    def vdm(self):
        # Compute Vandermonde matrix
        vdm = []

        # For each basis and coefficient
        for i, (coeff, bas) in enumerate(zip(self.coeffs, self.monobasis)):
            # Evaluate the basis at each point
            prod = [[c*np.prod(np.power(pt, b)) for b, c in zip(bas, coeff)]
                    for pt in self.pts]

            # Dot by the normal of the points
            a_nk = np.einsum('ij, ij -> i', prod, self.dofs)

            # Extend
            vdm.extend([a_nk])

        eps = 1e-13
        vdm = np.array(vdm)
        vdm[np.abs(vdm) < eps] = 0.0
        return np.array(vdm)

    def div_basis_at_py(self, point):
        der = np.zeros(len(self.monobasis))

        # Compute the divergence of each basis
        for i, (coeff, basis) in enumerate(zip(self.coeffs, self.monobasis)):
            for dim, c in zip(range(self.ndim), coeff):
                bas = np.array(basis[dim])
                if bas[dim] >= 1:
                    bas[dim] -= 1
                    der[i] += (bas[dim] + 1)*np.prod(np.power(point, bas))*c

        # Multiply by the inverse Vandermonde matrix
        # Use linalg solve for better accuracy
        return np.linalg.solve(self.vdm, der)

    @clean
    def div_basis_at(self, pts):
        if len(pts) and not isinstance(pts[0], Iterable):
            pts = [(p,) for p in pts]

        return np.array([self.div_basis_at_py(p).T for p in pts])

    @lazyprop
    @clean
    def invvdm(self):
        return np.linalg.inv(self.vdm)

    def coeffs_rt(self, rts):
        # Allow to distinguish basis that are 0, from those
        # as (x^0, y^0) is not zero
        # Look for basis which have (0, 0) in a given dimension
        # Annotate coeff = 0 for these basis
        # If (0, 0) in all the dimensions then this basis is the
        # zeroth degree, and therefore annotate (1, 0) or
        # (0, 1) depending on the order
        coeffs = [[1 if b != (0,)*self.ndim else 0 for b in bas]
                  for bas in rts]
        coeffs = np.array(coeffs)
        i = 0
        for j, rt in enumerate(rts):
            cond = [rt[dim] == (0, )*self.ndim for dim in range(self.ndim)]
            if np.all(cond):
                coeffs[j][i] = 1
                i += 1
        return coeffs

    def test_vdm(self):
        self.vdm
        for i, pt in enumerate(self.pts):
            vals = np.zeros((len(self.monobasis), self.ndim))
            for j, bas in enumerate(self.monobasis):
                coeff = self.coeffs[j]
                for dim in range(self.ndim):
                    vals[j, dim] = coeff[dim]*np.prod(np.power(pt, bas[dim]))
            vals = np.einsum('ij, j->i', vals, self.dofs[i])
            assert(abs(np.sum(self.invvdm @ vals) - 1.0) < 1e-9)


class TriRTPolyBasis(BaseRTPolyBasis):
    name = 'tri'

    ndim = 2

    @lazyprop
    def monobasis(self):
        # Find the default bases of tri elements
        # x, y, x^2, xy, y^2, etc.
        basis1d = [(i, j)
                   for i in range(self.order)
                   for j in range(self.order - i)]

        # Copy it for each dimension
        # adding zeros in opposite dimensions
        # See Viellieux2020
        zeroth = (0, )*self.ndim
        rts = [[bas, zeroth] for bas in basis1d]
        rts.extend([[zeroth, bas] for bas in basis1d])

        # Find basis which add up to degree
        rt = [bas for bas in basis1d if sum(bas) == self.order - 1]

        # For each dimension, add the correpondent RT extra bases
        a = np.eye(self.ndim, dtype=np.int)
        for r in rt:
            rts.extend([[tuple(r + a[i]) for i in range(self.ndim)]])

        self.coeffs = self.coeffs_rt(rts)

        return rts

    @clean
    def ortho_basis_at(self, pts):
        if len(pts) and not isinstance(pts[0], Iterable):
            pts = [(p,) for p in pts]

        return np.array([self.ortho_basis_at_py(*p) for p in pts]).T

    def ortho_basis_at_py(self, p, q):
        # See:
        # Efficient Equilibrated Flux Reconstruction
        # in High Order Raviart-Thomas Space for
        # Discontinuous Galerkin methods
        r = p
        s = q
        jac = jacobi_norm
        res = []

        # Edges
        a = s
        rb = 0.5*np.array([1 + r, 1 + s])
        for i, pi in enumerate(jac(self.order - 1, 0, 0, a)):
            res.append([pi*rb[m] for m in range(self.ndim)])

        rb = 0.5*np.array([r - 1, s + 1])
        for i, pi in enumerate(jac(self.order - 1, 0, 0, a)):
            res.append([pi*rb[m] for m in range(self.ndim)])

        a = r
        rb = 0.5*np.array([1 + r, q - 1])
        for i, pi in enumerate(jac(self.order - 1, 0, 0, a)):
            res.append([pi*rb[m] for m in range(self.ndim)])

        # Dubiner basis (from polys)
        a = 2*(1 + p)/(1 - q) - 1 if q != 1 else -1
        b = q

        ts = 0.5*(s + 1)*np.array([r + 1, s - 1])
        for i, pi in enumerate(jacobi(self.order - 2, 0, 0, a)):
            ci = (1 - b)**i
            for j, pj in enumerate(jacobi(self.order - i - 2, 2*i + 1, 0, b)):
                cij = sqrt((2*i + 1)*(2*i + 2*j + 2)) / 2**(i + 1)
                coeff = pi*ci*pj*cij
                res.append([coeff*t for t in ts])

        ts = 0.5*(r + 1)*np.array([r - 1, s + 1])
        for i, pi in enumerate(jacobi(self.order - 2, 0, 0, a)):
            ci = (1 - b)**i
            for j, pj in enumerate(jacobi(self.order - i - 2, 2*i + 1, 0, b)):
                cij = sqrt((2*i + 1)*(2*i + 2*j + 2)) / 2**(i + 1)
                coeff = pi*ci*pj*cij
                res.append([coeff*t for t in ts])

        return res

    def div_basis_at_py_v2(self, p, q):
        r = p
        s = q
        jac = jacobi_norm
        jac_diff = jacobi_diff_norm
        J = jacobian(self.ortho_basis_at_py, [p, q])
        res = []

        def jac_ortho_basis_edges(a, da, rb, drb):
            f = jac(self.order - 1, 0, 0, a)
            df = jac_diff(self.order - 1, 0, 0, a)

            ret = []
            for i, (fi, dfi) in enumerate(zip(f, df)):
                ds = [da[k]*dfi*rb[k] + fi*drb[k] for k in range(self.ndim)]
                ret.append(ds)
            return np.array(ret)

        def jac_ortho_basis_int(a, b, da, db, t, dt):
            f = jacobi(self.order - 2, 0, 0, a)
            df = jacobi_diff(self.order - 2, 0, 0, a)
            ret = []
            for i, (fi, dfi) in enumerate(zip(f, df)):
                ci = (1 - b)**i
                dci = [-d*i*(1 - b)**(i - 1) if i != 0 else 0 for d in db]

                g = jacobi(self.order - i - 2, 2*i + 1, 0, b)
                dg = jacobi_diff(self.order - i - 2, 2*i + 1, 0, b)
                for j, (gj, dgj) in enumerate(zip(g, dg)):
                    cij = sqrt((2*i + 1)*(2*i + 2*j + 2)) / 2**(i + 1)
                    ds = []
                    for k in range(self.ndim):
                        ds.append(da[k]*dfi*ci*gj*cij*t[k]
                                  + fi*dci[k]*gj*cij*t[k]
                                  + fi*ci*db[k]*dgj*cij*t[k]
                                  + fi*ci*gj*cij*dt[k])
                    ret.append(ds)
            return np.array(ret)

        # Edges
        a = s
        rb = 0.5*np.array([1 + r, 1 + s])
        da = [0.0, 1.0]
        drb = [0.5, 0.5]
        res.extend(jac_ortho_basis_edges(a, da, rb, drb))

        rb = 0.5*np.array([r - 1, s + 1])
        drb = 0.5*np.array([1, 1])
        res.extend(jac_ortho_basis_edges(a, da, rb, drb))

        a = r
        da = [1.0, 0.0]
        rb = 0.5*np.array([1 + r, q - 1])
        drb = 0.5*np.array([1, 1])
        res.extend(jac_ortho_basis_edges(a, da, rb, drb))

        # Dubiner basis (from polys)
        a = 2*(1 + p)/(1 - q) - 1 if q != 1 else -1
        b = q

        da = [2/(1 - q) if q != 1 else 0, 2*(1 + p)/(1 - q)**2 if q != 1 else 0]
        db = [0, 1]

        ts = 0.5*(s + 1)*np.array([r + 1, s - 1])
        dts = [0.5*(s + 1), s]
        res.extend(jac_ortho_basis_int(a, b, da, db, ts, dts))

        ts = 0.5*(r + 1)*np.array([r - 1, s + 1])
        dts = [r, 0.5*(r + 1)]
        res.extend(jac_ortho_basis_int(a, b, da, db, ts, dts))

        # Compute the divergence
        res = np.array(res).sum(axis=1)

        return res

    @clean
    def div_basis_at(self, pts):
        if len(pts) and not isinstance(pts[0], Iterable):
            pts = [(p,) for p in pts]

        div = super().div_basis_at(pts)
        a = np.array([self.div_basis_at_py_v2(*p) for p in pts])
        div2 = np.linalg.solve(self.vdm_v2, a.T).T

        print(f'{np.max(np.abs(div - div2))=}')
        print(f'{np.linalg.cond(self.vdm_v2)=}, {np.linalg.cond(self.vdm)=}')
        return div2

    @lazyprop
    def vdm_v2(self):
        # Compute Vandermonde matrix
        prod = self.ortho_basis_at(self.pts)
        return np.einsum('ijk, ki -> jk', prod, self.dofs)

class TetRTPolyBasis(TriRTPolyBasis):
    name = 'tet'

    ndim = 3

    @lazyprop
    def monobasis(self):
        # Find the default bases of tet elements
        # x, y, x^2, xy, y^2, etc.
        basis1d = [(i, j, k)
                   for i in range(self.order)
                   for j in range(self.order - i)
                   for k in range(self.order - i - j)]

        # Copy it for each dimension
        # adding zeros in opposite dimensions
        # See Viellieux2020
        zeroth = (0, )*self.ndim
        rts = [[bas, zeroth, zeroth] for bas in basis1d]
        rts.extend([[zeroth, bas, zeroth] for bas in basis1d])
        rts.extend([[zeroth, zeroth, bas] for bas in basis1d])

        # Find basis which add up to degree
        rt = [bas for bas in basis1d if sum(bas) == self.order - 1]

        # For each dimension, add the correpondent RT extra bases
        a = np.eye(self.ndim, dtype=np.int)
        for r in rt:
            rts.extend([[tuple(r + a[i]) for i in range(self.ndim)]])

        self.coeffs = self.coeffs_rt(rts)

        return rts

    def ortho_basis_at_py(self, p, q, r):
        x = 0.5*(1 + p)
        y = 0.5*(1 + q)
        z = 0.5*(1 + r)

        jac = jacobi

        bas = []

        a = 2*x/(1 - y) - 1 if y != 1 else -1
        b = 2*y - 1
        for i, pi in enumerate(jac(self.order - 1, 0, 0, a)):
            ci = (1 - y)**i
            for j, pj in enumerate(jac(self.order - i - 1, 2*i + 1, 0, b)):
                coeff = pj*ci*pi
                bas.append((-coeff*x, -coeff*y, coeff*(1 - z)))

        a = 2*x/(1 - z) - 1 if z != 1 else -1
        b = 2*z - 1
        for i, pi in enumerate(jac(self.order - 1, 0, 0, a)):
            ci = (1 - z)**i
            for j, pj in enumerate(jac(self.order - i - 1, 2*i + 1, 0, b)):
                coeff = pj*ci*pi
                bas.append((coeff*x, coeff*(y - 1), coeff*z))

        a = 2*y/(1 - z) - 1 if z != 1 else -1
        b = 2*z - 1
        for i, pi in enumerate(jac(self.order - 1, 0, 0, a)):
            ci = (1 - z)**i
            for j, pj in enumerate(jac(self.order - i - 1, 2*i + 1, 0, b)):
                coeff = pj*ci*pi
                bas.append((coeff*(1 - x), -coeff*y, -coeff*z))

        for i, pi in enumerate(jac(self.order - 1, 0, 0, a)):
            ci = (1 - z)**i
            for j, pj in enumerate(jac(self.order - i - 1, 2*i + 1, 0, b)):
                coeff = pj*ci*pi
                bas.append((coeff*x, coeff*y, coeff*z))

        # TODO if condition
        a = 2*x/(1 - y - z) - 1 if r != -q else -1
        b = 2*y/(1 - z) - 1 if z != 1 else -1
        c = 2*z - 1
        for i, pi in enumerate(jac(self.order - 2, 0, 0, a)):
            ci = (1 - y - z)**i

            for j, pj in enumerate(jac(self.order - i - 2, 2*i + 1, 0, b)):
                cj = (1 - z)**j

                jp = jac(self.order - i - j - 2, 2*(i + j + 1) + 1, 0, c)
                for k, pk in enumerate(jp):
                    coeff = (ci*pi*cj*pj*pk)
                    bas.append((coeff*x*z, coeff*y*z, coeff*z*(z - 1)))
                    bas.append((coeff*x*y, coeff*y*(y - 1), coeff*y*z))
                    bas.append((coeff*x*(x - 1), coeff*x*y, coeff*x*z))

        return bas

    def div_basis_at_py_v2(self, p, q, r):
        x = 0.5*(1 + p)
        y = 0.5*(1 + q)
        z = 0.5*(1 + r)

        jac = jacobi
        jac_diff = jacobi_diff

        res = []

        # 1st
        a = 2*x/(1 - y) - 1 if y != 1 else -1
        b = 2*y - 1
        rb = [-x, -y, 1 - z]

        da = [2/(1 - y), 2*x/(1 - y)**2, 0.0]
        db = [0.0, 2.0, 0.0]
        drb = [-1.0, -1.0, -1.0]

        cifunc = lambda x, y, z, i, j: (1 - y)**i
        dcifunc = lambda x, y, z, i, j: [0.0, -i*(1 - y)**(i - 1) if i > 0 else 0, 0.0]
        res.extend(self.jac_ortho_basis_faces(x, y, z, a, b, da, db, cifunc, dcifunc, rb, drb))

        # 2nd
        a = 2*x/(1 - z) - 1 if z != 1 else -1
        b = 2*z - 1
        rb = [x, y - 1, z]

        da = [2/(1 - z), 0.0, 2*x/(1 - z)**2]
        db = [0.0, 0.0, 2.0]
        drb = [1.0, 1.0, 1.0]

        cifunc = lambda x, y, z, i, j: (1 - z)**i
        dcifunc = lambda x, y, z, i, j: [0.0, 0.0, -i*(1 - z)**(i - 1) if i > 0 else 0]
        res.extend(self.jac_ortho_basis_faces(x, y, z, a, b, da, db, cifunc, dcifunc, rb, drb))

        # 3rd
        a = 2*y/(1 - z) - 1 if z != 1 else -1
        rb = [1 - x, -y, -z]

        da = [0.0, 2/(1 - z), 2*y/(1 - z)**2]
        drb = [-1.0, -1.0, -1.0]

        res.extend(self.jac_ortho_basis_faces(x, y, z, a, b, da, db, cifunc, dcifunc, rb, drb))

        # 4th
        rb = [x, y, z]
        drb = [1.0, 1.0, 1.0]
        res.extend(self.jac_ortho_basis_faces(x, y, z, a, b, da, db, cifunc, dcifunc, rb, drb))

        # TODO if condition
        a = 2*x/(1 - y - z) - 1 if r != -q else -1
        b = 2*y/(1 - z) - 1 if z != 1 else -1
        c = 2*z - 1

        da = [2/(1 - y - z) if r != -q else 0,
              2*x/(1 - y - z)**2 if r != -q else 0,
              2*x/(1 - y - z)**2 if r != -q else 0]
        db = [0,
              2/(1 - z) if z != 1 else 0,
              2*y/(1 - z)**2 if z != 1 else 0]
        dc = [0.0, 0.0, 2.0]

        rb = [
             [x*z, y*z, z*(z - 1)],
             [x*y, y*(y - 1), y*z],
             [x*(x - 1), x*y, x*z]
             ]

        drb = [
              [z, z, 2*z - 1],
              [y, 2*y - 1, y],
              [2*x - 1, x, x]
              ]

        f = jac(self.order - 2, 0, 0, a)
        df = jac_diff(self.order - 2, 0, 0, a)
        for i, (fi, dfi) in enumerate(zip(f, df)):
            ci = (1 - y - z)**i
            dci = -i*(1 - y - z)**(i - 1) if i != 0 else 0
            dci = [0.0, dci, dci]

            g = jac(self.order - i - 2, 2*i + 1, 0, b)
            dg = jac_diff(self.order - i - 2, 2*i + 1, 0, b)
            for j, (gj, dgj) in enumerate(zip(g, dg)):
                cj = (1 - z)**j
                dcj = [0.0, 0.0, -j*(1 - z)**(j - 1) if j != 0 else 0]

                jp = jac(self.order - i - j - 2, 2*(i + j + 1) + 1, 0, c)
                djp = jac_diff(self.order - i - j - 2, 2*(i + j + 1) + 1, 0, c)
                for k, (pk, dpk) in enumerate(zip(jp, djp)):
                    for n in range(self.ndim):
                        ds = []
                        for m in range(self.ndim):
                            ds.append(
                              da[m]*dfi*ci*gj*cj*pk*rb[n][m]
                              + fi*dci[m]*gj*cj*pk*rb[n][m]
                              + fi*ci*db[m]*dgj*cj*pk*rb[n][m]
                              + fi*ci*gj*dcj[m]*pk*rb[n][m]
                              + fi*ci*gj*cj*dc[m]*dpk*rb[n][m]
                              + fi*ci*gj*cj*pk*drb[n][m])
                        # Take into account that the derivative
                        # with x, y, z is half that with p, q, r
                        res.append([0.5*val for val in ds])

        # Compute the divergence
        res = np.array(res).sum(axis=1)

        return res

    def jac_ortho_basis_faces(self, x, y, z, a, b, da, db, cifunc, dcifunc, r, dr):
        jac = jacobi
        jac_diff = jacobi_diff

        f = jac(self.order - 1, 0, 0, a)
        df = jac_diff(self.order - 1, 0, 0, a)

        res = []
        for i, (fi, dfi) in enumerate(zip(f, df)):
            ci = cifunc(x, y, z, i, 0)
            dci = dcifunc(x, y, z, i, 0)

            g = jac(self.order - i - 1, 2*i + 1, 0, b)
            dg = jac_diff(self.order - i - 1, 2*i + 1, 0, b)
            for j, (gj, dgj) in enumerate(zip(g, dg)):
                # dx
                ds = []
                for k in range(self.ndim):
                    ds.append(da[k]*dfi*ci*gj*r[k] + fi*dci[k]*gj*r[k]
                              + fi*ci*db[k]*dgj*r[k] + fi*ci*gj*dr[k])
                res.append(ds)
        # Take into account that the derivative
        # with x, y, z is half that with p, q, r
        return np.array(res)/2.0


class QuadRTPolyBasis(BaseRTPolyBasis):
    # TODO: use coeffs of lagrange basis
    # to avoid badly conditioned VDM
    name = 'quad'

    ndim = 2

    @lazyprop
    def monobasis(self):
        # Find the default bases of quad elements
        # x, y, x^2, xy, y^2, etc.
        basis1d = [(i, j)
                   for i in range(self.order + 1)
                   for j in range(self.order)]

        # Copy it for each dimension
        # adding zeros in opposite dimensions
        # See Viellieux2020
        zeroth = (0, )*self.ndim
        rts = [[bas, zeroth] for bas in basis1d]
        rts.extend([[zeroth, (bas[1], bas[0])] for bas in basis1d])

        # TODO: use maybe Lagrange coeffs at some point...
        self.coeffs = self.coeffs_rt(rts)

        return rts

    @clean
    def ortho_basis_at(self, pts):
        if len(pts) and not isinstance(pts[0], Iterable):
            pts = [(p,) for p in pts]

        return np.array([self.ortho_basis_at_py(*p) for p in pts]).T

    def ortho_basis_at_py(self, p, q):
        # First set of tensor-product vector monomials
        # Watch out, the ordering of the for loops
        # must coincide with that used in div_basis
        # res_xi = [(pi*pj, 0) for pi in pa for pj in pb]
        res_xi = [(pi*pj, 0)
                  for pi in jacobi_norm(self.order, 0, 0, p)
                  for pj in jacobi_norm(self.order - 1, 0, 0, q)]

        res_eta = [(0, pi*pj)
                   for pi in jacobi_norm(self.order - 1, 0, 0, p)
                   for pj in jacobi_norm(self.order, 0, 0, q)]
        return np.vstack([res_xi, res_eta])

    def div_basis_at_py_v2(self, p, q):
        # Watch out, the ordering of the for loops
        # must coincide with that used in ortho_basis
        # res_xi = [dpi*pj for dpi in dpa for pj in pb]
        res_xi = [dpi*pj
                  for dpi in jacobi_diff_norm(self.order, 0, 0, p)
                  for pj in jacobi_norm(self.order - 1, 0, 0, q)]
        # Eta-Derivative
        # Watch out, the ordering of the for loops
        # must coincide with that used in ortho_basis
        res_eta = [dpi*pj
                   for pj in jacobi_norm(self.order - 1, 0, 0, p)
                   for dpi in jacobi_diff_norm(self.order, 0, 0, q)]

        return np.hstack([res_xi, res_eta])

    @clean
    def div_basis_at_v2(self, pts):
        if len(pts) and not isinstance(pts[0], Iterable):
            pts = [(p,) for p in pts]

        a = np.array([self.div_basis_at_py_v2(*p) for p in pts])
        return np.linalg.solve(self.vdm_v2, a.T).T

    @lazyprop
    def vdm_v2(self):
        # Compute Vandermonde matrix
        prod = self.ortho_basis_at(self.pts)
        return np.einsum('ijk, ki -> jk', prod, self.dofs)

    @lazyprop
    def idxs_zero_col(self):
        # https://stackoverflow.com/questions/26864619/inserting-rows-and-columns-into-a-numpy-array
        nfintpts_dim = len(self.ptsint) // self.ndim
        idxs = []
        for i in range(self.ndim - 1):
            idxs.extend([nfintpts_dim*(i + 1) + i*self.ndim*nfintpts_dim + j
                         for j in range(nfintpts_dim*self.ndim)])
        idxs = idxs - np.arange(len(idxs))
        return idxs

    def div_basis_at(self, pts):
        # TensorProduct elements present an issue as each internal flux point
        # is only assigned to a unique dof
        # As the SDRT implementation supposes that each internal flux point
        # contributes to ndim dofs, we need to add zero columns
        # to take into account the ndim - 1 dofs that the SDRT method
        # does not use to build the VdM matrices and etc,
        # YES this is not optimal...
        div = super().div_basis_at(pts)
        div2 = self.div_basis_at_v2(pts)
        print(f'{np.max(np.abs(div - div2))=}')
        print(f'{np.linalg.cond(self.vdm_v2)=}, {np.linalg.cond(self.vdm)=}')
        return np.insert(div2, self.idxs_zero_col, 0, axis=1)


class HexRTPolyBasis(QuadRTPolyBasis):
    # TODO: use coeffs of lagrange basis
    # to avoid badly conditioned VDM
    name = 'hex'

    ndim = 3

    @lazyprop
    def monobasis(self):
        # Find the default bases of hex elements
        # x, y, x^2, xy, y^2, etc.
        basis1d = [(i, j, k)
                   for i in range(self.order + 1)
                   for j in range(self.order)
                   for k in range(self.order)]

        # Copy it for each dimension
        # adding zeros in opposite dimensions
        # See Viellieux2020
        zeroth = (0, )*self.ndim
        rts = [[bas, zeroth, zeroth] for bas in basis1d]
        rts.extend([[zeroth, (bas[1], bas[0], bas[2]), zeroth]
                    for bas in basis1d])
        rts.extend([[zeroth, zeroth, (bas[2], bas[1], bas[0])]
                    for bas in basis1d])

        # TODO: use maybe Lagrange coeffs at some point...
        self.coeffs = self.coeffs_rt(rts)

        return rts

    def ortho_basis_at_py(self, p, q, r):
        # First set of tensor-product vector monomials
        # Watch out, the ordering of the for loops
        # must coincide with that used in div_basis
        res_xi = [(pi*pj*pk, 0, 0)
                  for pi in jacobi_norm(self.order, 0, 0, p)
                  for pj in jacobi_norm(self.order - 1, 0, 0, q)
                  for pk in jacobi_norm(self.order - 1, 0, 0, r)]

        res_eta = [(0, pi*pj*pk, 0)
                   for pi in jacobi_norm(self.order - 1, 0, 0, p)
                   for pj in jacobi_norm(self.order, 0, 0, q)
                   for pk in jacobi_norm(self.order - 1, 0, 0, r)]

        res_zeta = [(0, 0, pi*pj*pk)
                    for pi in jacobi_norm(self.order - 1, 0, 0, p)
                    for pj in jacobi_norm(self.order - 1, 0, 0, q)
                    for pk in jacobi_norm(self.order, 0, 0, r)]

        return np.vstack([res_xi, res_eta, res_zeta])

    def div_basis_at_py_v2(self, p, q, r):
        # Xi-Derivative
        # Watch out, the ordering of the for loops
        # must coincide with that used in ortho_basis
        res_xi = [dpi*pj*pk
                  for dpi in jacobi_diff_norm(self.order, 0, 0, p)
                  for pj in jacobi_norm(self.order - 1, 0, 0, q)
                  for pk in jacobi_norm(self.order - 1, 0, 0, r)]

        # Eta-Derivative
        # Watch out, the ordering of the for loops
        # must coincide with that used in ortho_basis
        res_eta = [dpi*pj*pk
                   for pj in jacobi_norm(self.order - 1, 0, 0, p)
                   for dpi in jacobi_diff_norm(self.order, 0, 0, q)
                   for pk in jacobi_norm(self.order - 1, 0, 0, r)]

        # Zeta-Derivative
        # Watch out, the ordering of the for loops
        # must coincide with that used in ortho_basis
        res_zeta = [dpi*pj*pk
                    for pj in jacobi_norm(self.order - 1, 0, 0, p)
                    for pk in jacobi_norm(self.order - 1, 0, 0, q)
                    for dpi in jacobi_diff_norm(self.order, 0, 0, r)]

        return np.hstack([res_xi, res_eta, res_zeta])


class PriRTPolyBasis(BaseRTPolyBasis):
    # TODO: use coeffs of lagrange basis
    # to avoid badly conditioned VDM
    name = 'pri'

    ndim = 3

    @lazyprop
    def monobasis(self):
        # High-Order Optimal Edge Elements for Pyramids,
        # Prisms and Hexahedra
        # Basis of triangles but with 0 in third dimension
        basis1d = [(i, j, 0)
                   for i in range(self.order)
                   for j in range(self.order - i)]

        # Copy it for each dimension
        # adding zeros in opposite dimensions
        # See Viellieux2020
        zeroth = (0, )*self.ndim
        rts = [[bas, zeroth, zeroth] for bas in basis1d]
        rts.extend([[zeroth, bas, zeroth] for bas in basis1d])

        # Find basis which add up to degree
        rt = [bas for bas in basis1d if sum(bas) == self.order - 1]

        # For each dimension, add the correpondent RT extra bases
        a = np.eye(self.ndim, dtype=np.int)
        a[2, :] = 0
        for r in rt:
            rts.extend([[tuple(r + a[i]) if i < self.ndim - 1 else zeroth for i in range(self.ndim)]])

        # Placeholder coeff which allows
        # to distinguish between bases 1 and 0
        coeffs = self.coeffs_rt(rts)

        # Multiply RT tri basis by Lagrange polynomial
        # but only those bases which are not zero
        import copy
        rts_c = copy.deepcopy(rts)
        for o in range(1, self.order):
            rts_cpy = copy.deepcopy(rts_c)
            for i in range(len(rts_cpy)):
                for j in range(self.ndim - 1):
                    if coeffs[i][j] != 0:
                        val = list(rts_cpy[i][j])
                        val[2] = o
                        rts_cpy[i][j] = tuple(val)
            rts.extend(rts_cpy)

        # Add tensor-product bases with Lagrange Polynomial
        # multiplying default tri basis
        # See Approximation of H(div) with High-Order Optimal
        # Finite Elements for Pyramids, Prisms and Hexahedra
        # Tensor-product of tri base and Lagrange base
        basisz = [(i, j, k)
                  for i in range(self.order)
                  for j in range(self.order - i)
                  for k in range(self.order + 1)]
        rts.extend([[zeroth, zeroth, bas] for bas in basisz])

        # Update the coeffs
        # to take into account the new tensor-product bases
        # TODO: use maybe Lagrange coeffs at some point...
        self.coeffs = self.coeffs_rt(rts)

        # Compute the number of internal flux points which come
        # from the triangle polygon repetition
        # and from SDRT triangle internal flux points

        # These ones have only [0, 0, 1] as dof
        self.nfintpts_tris = len(np.where((self.dofsint == (0, 0, 1))
                                 .all(axis=1))[0])

        # These ones may have [1, 0, 0] or [0, 1, 0]
        self.nfintpts_notris = len(np.where((self.dofsint == (1, 0, 0))
                                   .all(axis=1))[0])

        return rts

    @lazyprop
    def idxs_zero_col(self):
        # https://stackoverflow.com/questions/26864619/inserting-rows-and-columns-into-a-numpy-array
        # See div_basis_at to understand why we need to pad with zeros...
        idxs = []
        a = self.nfintpts_notris
        b = self.nfintpts_tris

        last = a
        idxs.extend(range(last, last + b))
        last = idxs[-1] + a + 1
        idxs.extend(range(last, last + b))
        last = idxs[-1] + 1
        idxs.extend(range(last, last + a))

        idxs = idxs - np.arange(len(idxs))
        return idxs

    @clean
    def div_basis_at(self, pts):
        # Pri elements present an issue as each internal flux point
        # is only assigned to one or two dofs, but not to ndim (3) dofs
        # As the SDRT implementation supposes that each internal flux point
        # contributes to ndim dofs, we need to add zero columns
        # to take into account the dofs that the SDRT method
        # does not use to build the VdM matrices and etc,
        # YES this is not optimal...
        div = super().div_basis_at(pts)
        return np.insert(div, self.idxs_zero_col, 0, axis=1)


class PyrRTPolyBasis(BaseRTPolyBasis):
    name = 'pyr'

    ndim = 3

    @lazyprop
    def monobasis(self):
        # High-Order Optimal Edge Elements for Pyramids,
        # Prisms and Hexahedra
        # Find the default bases of pry elements
        # Here we need to add an extra dimension
        # To take into account the (1 - z) terms...
        # This is going to be complex...
        basis1d = [(i, j, k, 0)
                   for i in range(self.order)
                   for j in range(self.order - i)
                   for k in range(self.order - i - j)]

        for o in range(self.order - 1):
            bas = [(i, j, k, 0)
                   for i in range(o)
                   for j in range(o - i)
                   for k in range(o - i - j)]
            for i, b in enumerate(bas):
                arr = list(b)
                d = self.order - 1 - o
                arr[0] += d
                arr[1] += d
                arr[4] -= d
                bas[i] = tuple(arr)
            basis1d.extend(bas)

        # Copy it for each dimension
        # adding zeros in opposite dimensions
        # See Viellieux2020
        zeroth = (0, )*(self.ndim + 1)
        rts = [[bas, zeroth, zeroth] for bas in basis1d]
        rts.extend([[zeroth, bas, zeroth] for bas in basis1d])
        rts.extend([[zeroth, zeroth, bas] for bas in basis1d])
        raise RuntimeError('PyrRTPolyBasis not implemented')
